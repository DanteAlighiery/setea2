import 'package:flutter/material.dart';
import 'Shop.dart';
import '../setea_icons_icons.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import '../Components/Bigteacup.dart';

final myController = TextEditingController();

TextEditingController streetController = TextEditingController();
TextEditingController domController = TextEditingController();
TextEditingController kvController = TextEditingController();
TextEditingController floorController = TextEditingController();
TextEditingController commentController = TextEditingController();
TextEditingController nameController = TextEditingController();
TextEditingController numberCardController = TextEditingController();
TextEditingController dateController = TextEditingController();
TextEditingController cvvController = TextEditingController();

class Cart extends StatefulWidget {
  @override
  createState() => new CartState();
}

class CartState extends State<Cart> {
  bool buy = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          bottomOpacity: 0,
          elevation: 0,
          backgroundColor: Color(0xFFFFFFFF),
          leading: IconButton(
            icon:
                const Icon(AntDesign.left, color: Color(0xFF273522), size: 30),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
        body: Container(
          color: Color(0xFFFFFFFF),
          padding: EdgeInsets.symmetric(horizontal: 18),
          child: ListView(
            scrollDirection: Axis.vertical,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Корзина',
                    style: TextStyle(
                        color: Color(0xFF303030),
                        fontSize: 34,
                        fontWeight: FontWeight.w700),
                  ),
                  SizedBox(
                    height: 18,
                  ),
                  BuyCard(),
                  (buy==false)?
                  Column(
                    children: <Widget>[
                      Text(
                        'Выберете способ оплаты',
                        style: TextStyle(
                            color: Color(0xFF4F4F4F),
                            fontSize: 24,
                            fontWeight: FontWeight.w400),
                      ),
                      SizedBox(
                        height: 18,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          Way(
                            color: Color(0xFF273522),
                            colorText: Colors.white,
                          ),
                          Way(
                            title: 'Оплата наличными',
                            imageurl: 'assets/images/money.png',
                            color: Color(0xFFF8F8F8),
                            colorText: Colors.black,
                          ),
                        ],
                      ),
                      Theme(
                          data: new ThemeData(
                            primaryColor: Colors.green,
                            primaryColorDark: Colors.green,
                          ),
                          child: Column(
                            children: <Widget>[

                              TextField(
                                controller: nameController,
                                decoration: InputDecoration(
                                  hoverColor: Colors.green,
                                  labelText: 'Имя на карте',
                                  suffixIcon: Icon(SeteaIcons.pencil),
                                ),
                              ),
                              TextField(
                                controller: numberCardController,
                                decoration: InputDecoration(
                                  hoverColor: Colors.green,
                                  labelText: 'Номер карты',
                                  suffixIcon: Icon(SeteaIcons.pencil),
                                ),
                              ),
                              TextField(
                                controller: dateController,
                                decoration: InputDecoration(
                                  hoverColor: Colors.green,
                                  labelText: 'Срок действия карты',
                                  suffixIcon: Icon(SeteaIcons.pencil),
                                ),
                              ),
                              TextField(
                                controller: cvvController,
                                decoration: InputDecoration(
                                  hoverColor: Colors.green,
                                  labelText: 'CVC кол',
                                  suffixIcon: Icon(SeteaIcons.pencil),
                                ),
                              ),
                            ],
                          )),
                      SizedBox(
                        height: 18,
                      ),
                      FlatButton(
                        shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(18.0),
                        ),
                        color: Color(0xFF273522),
                        textColor: Colors.white,
                        onPressed: () {
                          setState(() {
                            buy = true;
                          });
                        },
                        child: Container(
                            width: 350,
                            height: 50,
                            child: Center(
                              child: Text(
                                "Продолжить",
                                style: TextStyle(
                                  fontSize: 18,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            )),
                      ),
                    ],
                  ):

                  Column(
                    children: <Widget>[
                      Text(
                        'Выберете способ доставки',
                        style: TextStyle(
                            color: Color(0xFF4F4F4F),
                            fontSize: 24,
                            fontWeight: FontWeight.w400),
                      ),
                      SizedBox(
                        height: 18,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          Way(
                            title: 'Самовывоз из бара',
                            imageurl: 'assets/images/bar.png',
                            color: Color(0xFF273522),
                            colorText: Colors.white,
                          ),
                          Way(
                            title: 'Доставка курьером',
                            imageurl: 'assets/images/glovo.png',
                            color: Color(0xFFF8F8F8),
                            colorText: Colors.black,
                          ),
                        ],
                      ),
                      Theme(
                          data: new ThemeData(
                            primaryColor: Colors.green,
                            primaryColorDark: Colors.green,
                          ),
                          child: Column(
                            children: <Widget>[
                              TextField(
                                controller: streetController,
                                decoration: InputDecoration(
                                  hoverColor: Colors.green,
                                  labelText: 'Улица',
                                  suffixIcon: Icon(SeteaIcons.pencil),
                                ),
                              ),
                              TextField(
                                controller: domController,
                                decoration: InputDecoration(
                                  hoverColor: Colors.green,
                                  labelText: 'Дом',
                                  suffixIcon: Icon(SeteaIcons.pencil),
                                ),
                              ),
                              TextField(
                                controller: kvController,
                                decoration: InputDecoration(
                                  hoverColor: Colors.green,
                                  labelText: 'Квартира/офис',
                                  suffixIcon: Icon(SeteaIcons.pencil),
                                ),
                              ),
                              TextField(
                                controller: floorController,
                                decoration: InputDecoration(
                                  hoverColor: Colors.green,
                                  labelText: 'Этаж',
                                  suffixIcon: Icon(SeteaIcons.pencil),
                                ),
                              ),
                              TextField(
                                controller: commentController,
                                decoration: InputDecoration(
                                  hoverColor: Colors.green,
                                  labelText: 'Комментарий к заказу',
                                  suffixIcon: Icon(SeteaIcons.pencil),
                                ),
                              ),
                            ],
                          )),
                      SizedBox(
                        height: 18,
                      ),
                      FlatButton(
                        shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(18.0),
                        ),
                        color: Color(0xFF273522),
                        textColor: Colors.white,
                        onPressed: () {
                          setState(() {
                            buy = true;
                          });
                        },
                        child: Container(
                            width: 350,
                            height: 50,
                            child: Center(
                              child: Text(
                                "Продолжить",
                                style: TextStyle(
                                  fontSize: 18,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            )),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 12,
                  )
                ],
              ),
            ],
          ),
        ));
  }
}

class BuyCard extends StatefulWidget {
  @override
  createState() => new BuyCardState();
}

class BuyCardState extends State<BuyCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 18),
      decoration: BoxDecoration(
          color: Color(0xFFF8F8F8), borderRadius: BorderRadius.circular(15)),
      padding: EdgeInsets.only(top: 12, bottom: 6, right: 12, left: 12),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(right: 12),
                width: 55,
                height: 55,
                decoration: BoxDecoration(
                  color: Color(0xFF273522),
                  shape: BoxShape.circle,
                ),
                child: Stack(
//                  fit: StackFit.expand,
                  overflow: Overflow.visible,
                  children: <Widget>[
                    Positioned(
                        left: 8,
                        top: -10,
                        child: Image.asset(
                          'assets/images/supercup.png',
                          width: 40,
                        )),
                    Positioned(
                        left: 35,
                        top: -14,
                        child: Container(
                            padding: EdgeInsets.all(3),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              shape: BoxShape.circle,
                            ),
                            child: Text(
                              'S',
                              style: TextStyle(
                                  color: Color(0xFF303030), fontSize: 18),
                            )))
                  ],
                ),
              ),
              Column(
                children: <Widget>[
                  Text(
                    'Toxic tea',
                    style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.w200,
                        color: Color(0xFF303030)),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.circle,
                        ),
                        padding: EdgeInsets.all(6),
                        margin: EdgeInsets.only(right: 12),
                        child: Text(
                          '-',
                          style:
                              TextStyle(color: Color(0xFF747474), fontSize: 24),
                        ),
                      ),
                      Text(
                        '1',
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w200,
                            color: Color(0xFF273522)),
                      ),
                      Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.circle,
                        ),
                        padding: EdgeInsets.all(6),
                        margin: EdgeInsets.only(left: 12),
                        child: Text(
                          '+',
                          style:
                              TextStyle(color: Color(0xFF747474), fontSize: 24),
                        ),
                      ),
                    ],
                  )
                ],
              )
            ],
          ),
          Column(
            children: <Widget>[
              Text(
                '\$5',
                style: TextStyle(
                    color: Color(0xFF273522),
                    fontSize: 24,
                    fontWeight: FontWeight.w700),
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                'Удалить',
                style: TextStyle(
                    color: Color(0xFFC7C7C7),
                    fontSize: 14,
                    fontWeight: FontWeight.w100),
              )
            ],
          )
        ],
      ),
    );
  }
}

class Way extends StatelessWidget {
  String _title;
  String _imageurl;
  Color _color;
  Color _colorText;

  Way(
      {String title = 'Оплата картой',
      Color color,
      Color colorText,
      String imageurl = 'assets/images/card.png'}) {
    _title = title;
    _imageurl = imageurl;
    _color = color;
    _colorText = colorText;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 165,
      height: 70,
      padding: EdgeInsets.only(left: 18, right: 6),
      decoration:
          BoxDecoration(color: _color, borderRadius: BorderRadius.circular(15)),
      child: Row(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(right: 12),
            width: 44,
            height: 44,
            decoration: BoxDecoration(
              color: Color(0xFFFFFFFF),
              shape: BoxShape.circle,
            ),
            child: Stack(
//                  fit: StackFit.expand,
              overflow: Overflow.visible,
              children: <Widget>[
                Positioned(
                    left: -8,
                    top: 0,
                    child: Image.asset(
                      _imageurl,
                      width: 44,
                    )),
              ],
            ),
          ),
          Flexible(
            child: Text(
              _title,
              style: TextStyle(color: _colorText, fontSize: 14),
            ),
          )
        ],
      ),
    );
  }
}
