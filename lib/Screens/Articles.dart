import 'package:flutter/material.dart';
import '../Components/CookiesBox.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';

class Articles extends StatefulWidget {
  @override
  createState() => new ArticlesState();
}

class ArticlesState extends State<Articles> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          flexibleSpace: Image(
            image: AssetImage('assets/images/articles_open.png'),
            fit: BoxFit.cover,
          ),
          title: Image.asset(
            'assets/images/setea.png',
            width: 90,
            alignment: Alignment.center,
          ),
          leading: IconButton(
            icon:
                const Icon(AntDesign.left, color: Color(0xFFFFFFFF), size: 30),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          actions: <Widget>[
            Container(
              margin: EdgeInsets.only(right: 18),
              child: Icon(
                AntDesign.hearto,
                color: Color(0xFFFFFFFF),
                size: 25,
              ),
            )
          ],
        ),
        body: Container(
            child: Stack(
          children: <Widget>[
            Positioned(
              child: Container(
                height: 100,
                decoration: BoxDecoration(
                    borderRadius: new BorderRadius.only(
                      topLeft: const Radius.circular(30),
                      topRight: const Radius.circular(30),
                    ),
                    color: Colors.black26),
                child: Text(
                    'Название статьи максимум в несколько строк на конце троиточие статьи максимум в несколько'),
              ),
            )
          ],
        )));
  }
}
