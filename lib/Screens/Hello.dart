import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:setea2/Screens/Settings.dart';
import 'package:setea2/setea_icons_icons.dart';
import '../Components/CircleItem.dart';
import '../Components/Profilehead.dart';
import '../Components/Teabox.dart';
import './Camera.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'dart:async';
import 'dart:io';
import 'package:camera/camera.dart';

var textStyle = TextStyle(fontSize: 45.0, color: Color(0xFF273522), fontWeight:
FontWeight.bold,fontFamily: 'Rubik' );
var textStyle1 = TextStyle(fontSize: 22.0, color: Colors.black, fontWeight:
FontWeight.w200, fontFamily: 'Rubik');
var textStyle2 = TextStyle(fontSize: 20.0, color: Colors.black54, fontWeight:
FontWeight.w100, fontFamily: 'Rubik');
var textStyle3 = TextStyle(fontSize: 16.0, color: Colors.green, fontWeight:
FontWeight.w100, fontFamily: 'Rubik');
var textStyle5 = TextStyle(fontSize: 22.0, color: Colors.black, fontWeight:
FontWeight.w500, fontFamily: 'Rubik');
var textStyle4 = TextStyle(fontSize: 26.0, color: Colors.green, fontWeight:
FontWeight.w900, fontFamily: 'Rubik');
var textStyle6 = TextStyle(fontSize: 16.0, color: Color(0xFFEAEAEA), fontWeight:
FontWeight.normal, fontFamily: 'Rubik');
var textStyle7 = TextStyle(fontSize: 20.0, color: Colors.green, fontWeight:
FontWeight.w200, fontFamily: 'Rubik');
var textStyle8 = TextStyle(fontSize: 20.0, color: Colors.black, fontWeight:
FontWeight.w200, fontFamily: 'Rubik');

class Hello extends StatefulWidget {
  final List<CameraDescription> cameras;
  Hello({this.cameras});

  @override
  createState() => new HelloState();
}

class HelloState extends State<Hello> {
  String _imagePath;
  Future openCamera() async {
    availableCameras().then((cameras) async {
      final imagePath = await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => CameraWidget(cameras),
        ),
      );
      setState(() {
        _imagePath = imagePath;
      });
    });
  }
  Widget capturedImageWidget(String imagePath) {
    return SizedBox.expand(
      child: Image.file(File(
        imagePath,
      )),
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:Container(
        alignment: Alignment.center,
            child:ListView(
              children: <Widget>[
              Container(
                margin: new EdgeInsets.only(top:12),
                child:Text('Привет', style: textStyle, textAlign: TextAlign.center,)
              ),
                Container(
                    margin: new EdgeInsets.only(top:5),
                child:Text('Добро пожаловать!', style: textStyle1,  textAlign: TextAlign.center)
              ),
                Container(
                 margin: new EdgeInsets.only(top:18),
                  child: Image.asset('assets/images/cuphello.png',height: MediaQuery.of(context).size.height*0.45, fit:BoxFit.contain ),
                ),
                const SizedBox(height: 45),
                Container(
                  padding: new EdgeInsets.symmetric(horizontal: 16),
                  margin: new EdgeInsets.only(bottom:40, top:MediaQuery.of(context).size.height*0.005),
                  child:ClipRRect(
                    borderRadius: BorderRadius.circular(25.0),
                    child:RaisedButton(
                      padding: new EdgeInsets.all(15),
                      textColor: Colors.white,
                      color: Colors.green,
                      onPressed: openCamera,
                      child: const Text(
                          'Продолжить',
                          style: TextStyle(fontSize: 20)
                      ),

                    ),),),
              ],
            ),
      ),
    );

  }}