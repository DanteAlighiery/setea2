import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../Components/CircleItem.dart';
import '../Components/CookiesBox.dart';
import '../Components/Biggerteacup.dart';
import '../Components/LinedItem.dart';
import '../Screens//Filter.dart';


import 'Shop.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';



var textStyle = TextStyle(
    fontSize: 32.0,
    color: Colors.black,
    fontWeight: FontWeight.w600,
    fontFamily: 'Rubik');
var textStyle1 = TextStyle(
    fontSize: 24.0,
    color: Colors.grey[850],
    fontWeight: FontWeight.w200,
    fontFamily: 'Rubik');
var textStyle2 = TextStyle(
    fontSize: 20.0,
    color: Colors.black54,
    fontWeight: FontWeight.w100,
    fontFamily: 'Rubik');
var textStyle3 = TextStyle(
    fontSize: 18.0,
    color: Colors.green,
    fontWeight: FontWeight.w100,
    fontFamily: 'Rubik');
var textStyle5 = TextStyle(
    fontSize: 24.0,
    color: Colors.black,
    fontWeight: FontWeight.w500,
    fontFamily: 'Rubik');
var textStyle6 = TextStyle(
    fontSize: 20.0,
    color: Colors.black54,
    fontWeight: FontWeight.w400,
    fontFamily: 'Rubik');
var textStyle7 = TextStyle(
    fontSize: 20.0,
    color: Colors.green,
    fontWeight: FontWeight.w200,
    fontFamily: 'Rubik');
var textStyle8 = TextStyle(
    fontSize: 20.0,
    color: Colors.black,
    fontWeight: FontWeight.w200,
    fontFamily: 'Rubik');

class Category extends StatefulWidget {
  Category({Key key, this.active: false, @required this.onChanged})
      : super(key: key);

  final bool active;
  final ValueChanged<bool> onChanged;
  @override
  createState() => new CategoryState();
}

class CategoryState extends State<Category> {
  bool _highlight = false;
  void _handleTap() {
    setState(() {
      _highlight = true;
    });
  }
  void _handleTap2() {
    setState(() {
      _highlight = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        bottomOpacity: 0,
        elevation: 0,
        backgroundColor: Color(0xFFFFFFFF),
        leading:IconButton(
          icon: const Icon(AntDesign.left, color: Color(0xFF273522), size: 30),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: <Widget>[
          IconButton(
              icon: const Icon(Feather.shopping_cart,
                  color: Color(0xFF273522), size: 25),
              onPressed: () {
                 }),
        ],
      ),
      body:Container(
        color:Colors.white,
      height: 2500,
      child: ListView(
        scrollDirection: Axis.vertical,
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 25.0, left: 12.0, bottom: 20),
                child: Text('Печенья', style: textStyle),
              ),
              Row(
                  children: <Widget>[
                    GestureDetector( onTap:_handleTap,
                      child:Icon(EvilIcons.navicon, color:_highlight
                          ? Color(0xFF273522) : Colors.grey, size: 30),),
      Container(margin: EdgeInsets.only(right: 10),
        child:GestureDetector(onTap:_handleTap2, child:Icon(Entypo.grid, color:_highlight
          ? Colors.grey : Color(0xFF273522), size: 30,),),)

                  ]
              )
      ]),
               CookiesBox( 'Супер новинка!','Печеньки со вкусом вашего настроения','assets/images/cup5.png'),
             Row(
                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                 children: <Widget>[
              Container(
                margin: EdgeInsets.only(top: 18.0, left: 12.0, bottom: 18.0),
                child: Text(
                  'Все товары',
                  style: textStyle5,
                ),
              ),
                 InkWell(
                   onTap: () {
                 Navigator.push(
                 context,
                 MaterialPageRoute(builder: (context) => Filter()),
                 );},
                   child:Container(
                   margin: EdgeInsets.only(top: 18.0, right: 18.0, bottom: 18.0),
                   child: Text(
                     'Фильтр',
                     style: textStyle6,
                   ),
                 ),),
                 ]
             ),
            ],
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 12),
          margin: EdgeInsets.only(bottom:34),
          child:!_highlight
              ? Wrap(
            direction: Axis.horizontal,
            children: <Widget>[
              Biggerteacup('Toxic Tea', '5', 'assets/images/supercup.png'),
              Biggerteacup('Cookies', '6', 'assets/images/cup5.png'),
              Biggerteacup('Cookies', '6', 'assets/images/cup5.png'),
              Biggerteacup('Cookies', '6', 'assets/images/cup5.png'),
              Biggerteacup('Cookies', '6', 'assets/images/cup5.png'),

            ],
          ) : Column(
            children: <Widget>[
LinedItem(),
LinedItem(),
LinedItem(),
LinedItem(),
LinedItem(),
            ],
          ),),

        ],
      ),
    ),);
  }
}
