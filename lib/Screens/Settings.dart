import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:setea2/Screens/Settings.dart';
import 'package:setea2/setea_icons_icons.dart';
import '../Components/CircleItem.dart';
import '../Components/Profilehead.dart';
import '../Components/Teabox.dart';
import '../Components/Textwlink.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';

var textStyle = TextStyle(
    fontSize: 32.0,
    color: Colors.black,
    fontWeight: FontWeight.w600,
    fontFamily: 'Rubik');
var textStyle1 = TextStyle(
    fontSize: 24.0,
    color: Colors.grey[850],
    fontWeight: FontWeight.w200,
    fontFamily: 'Rubik');
var textStyle2 = TextStyle(
    fontSize: 20.0,
    color: Colors.black54,
    fontWeight: FontWeight.w100,
    fontFamily: 'Rubik');
var textStyle3 = TextStyle(
    fontSize: 16.0,
    color: Colors.green,
    fontWeight: FontWeight.w100,
    fontFamily: 'Rubik');
var textStyle5 = TextStyle(
    fontSize: 22.0,
    color: Colors.black,
    fontWeight: FontWeight.w500,
    fontFamily: 'Rubik');
var textStyle4 = TextStyle(
    fontSize: 26.0,
    color: Colors.green,
    fontWeight: FontWeight.w900,
    fontFamily: 'Rubik');
var textStyle6 = TextStyle(
    fontSize: 16.0,
    color: Color(0xFFEAEAEA),
    fontWeight: FontWeight.normal,
    fontFamily: 'Rubik');
var textStyle7 = TextStyle(
    fontSize: 20.0,
    color: Colors.green,
    fontWeight: FontWeight.w200,
    fontFamily: 'Rubik');
var textStyle8 = TextStyle(
    fontSize: 20.0,
    color: Colors.black,
    fontWeight: FontWeight.w200,
    fontFamily: 'Rubik');

class Settings extends StatefulWidget {
  @override
  createState() => new SettingsState();
}

final myController = TextEditingController();

TextEditingController nameController = TextEditingController();
TextEditingController secondnameController = TextEditingController();
TextEditingController phoneController = TextEditingController();
TextEditingController emailController = TextEditingController();
TextEditingController cityController = TextEditingController();

class SettingsState extends State<Settings> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        bottomOpacity: 0,
        elevation: 0,
        centerTitle: true,
        backgroundColor: Color(0xFFFFFFFF),
        leading: IconButton(
          icon: const Icon(AntDesign.left, color: Color(0xFF273522), size: 30),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title:

          Image.asset(
            'assets/images/seteagreen.png',
            width: 90,
            alignment: Alignment.center,
          ),


      ),
      body: Container(
          decoration: BoxDecoration(color: Colors.white),
          child: ListView(
            children: <Widget>[
              Container(
                margin: new EdgeInsets.only(top: 24),
                alignment: Alignment.center,
                child: Container(
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey[400],
                        blurRadius: 15.0,
                        // has the effect of softening the shadow
                        spreadRadius: 3.5,
                        // has the effect of extending the shadow
                        offset: Offset(
                          3.0, // horizontal, move right 10
                          5.0, // vertical, move down 10
                        ),
                      )
                    ],
                  ),
                  child: ClipRRect(
                    borderRadius: new BorderRadius.circular(20.0),
                    child: Container(
                      color: Color(0xFF273522),
                      height: 200,
                      width: 200,
                      child: Icon(
                        SeteaIcons.photo,
                        size: 45,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                margin: new EdgeInsets.only(left: 16.0, right: 16, top: 20),
                decoration: const BoxDecoration(
                  border: Border(
                    bottom: BorderSide(width: 1.0, color: Colors.grey),
                  ),
                ),
              ),
              Container(
                margin: new EdgeInsets.symmetric(horizontal: 18),
                child: new Theme(
                  data: new ThemeData(
                    primaryColor: Color(0xFF273522),
                    primaryColorDark: Color(0xFF273522),
                  ),
                  child: TextField(
                    controller: nameController,
                    decoration: InputDecoration(
                        hoverColor: Colors.green,
                        labelText: 'Имя',
                        suffixIcon: Icon(SeteaIcons.pencil),
                        hintStyle: textStyle5),
                  ),
                ),
              ),
              Container(
                margin: new EdgeInsets.symmetric(horizontal: 18),
                child: new Theme(
                  data: new ThemeData(
                    primaryColor: Color(0xFF273522),
                    primaryColorDark: Color(0xFF273522),
                  ),
                  child: TextField(
                    controller: secondnameController,
                    decoration: InputDecoration(
                      hoverColor: Colors.green,
                      labelText: 'Фамилия',
                      suffixIcon: Icon(SeteaIcons.pencil),
                    ),
                  ),
                ),
              ),
              Container(
                margin: new EdgeInsets.symmetric(horizontal: 18),
                child: new Theme(
                  data: new ThemeData(
                    primaryColor: Color(0xFF273522),
                    primaryColorDark: Color(0xFF273522),
                  ),
                  child: TextField(
                    controller: phoneController,
                    decoration: InputDecoration(
                      hoverColor: Colors.green,
                      labelText: 'Телефон',
                      suffixIcon: Icon(SeteaIcons.pencil),
                    ),
                  ),
                ),
              ),
              Container(
                margin: new EdgeInsets.symmetric(horizontal: 18),
                child: new Theme(
                  data: new ThemeData(
                    primaryColor: Color(0xFF273522),
                    primaryColorDark: Color(0xFF273522),
                  ),
                  child: TextField(
                    controller: emailController,
                    decoration: InputDecoration(
                      hoverColor: Colors.green,
                      labelText: 'Электронная почта',
                      suffixIcon: Icon(SeteaIcons.pencil),
                    ),
                  ),
                ),
              ),
              Container(
                margin: new EdgeInsets.symmetric(horizontal: 18),
                child: new Theme(
                  data: new ThemeData(
                    primaryColor: Color(0xFF273522),
                    primaryColorDark: Color(0xFF273522),
                  ),
                  child: TextField(
                    controller: cityController,
                    decoration: InputDecoration(
                      hoverColor: Colors.green,
                      labelText: 'Город',
                      suffixIcon: Icon(SeteaIcons.pencil),
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 50),
              Container(
                decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage("assets/images/button.png"),
                      fit: BoxFit.fitWidth,
                    )
                ),
                padding: new EdgeInsets.symmetric(horizontal: 16),
                margin: new EdgeInsets.only(bottom: 50),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(25.0),

                  child: RaisedButton(
                    padding: new EdgeInsets.all(15),
                    textColor: Colors.white,
                    color: Color(0xFF273522),
                    onPressed: () {},
                    child:Container(
                  decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("assets/images/button.png"),
                  fit: BoxFit.fitWidth,
                )
              ),
                        child:Text('Сохранить', style: TextStyle(fontSize: 20)),)
                  ),
               ),
              ),
            ],
          )),
    );
  }
}
