import 'dart:async';
import 'dart:io';
import 'dart:convert';
import 'package:camera/camera.dart';
import 'package:flutter/cupertino.dart';
import 'camera_page.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:qrscan/qrscan.dart' as scanner;
import 'dart:typed_data';
import 'package:google_fonts/google_fonts.dart';

class Cameraras extends StatefulWidget {
  @override
  CamerarasState createState() => CamerarasState();
}

class CamerarasState extends State<Cameraras> {
  String _imagePath;
  String barcode = '';
  Uint8List bytes = Uint8List(200);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          _imagePath != null
              ? capturedImageWidget(_imagePath)
              : noImageWidget(),
          fabWidget(),
        ],
      ),
    );
  }

  Widget noImageWidget() {
    return SizedBox.expand(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              child: Icon(
                Icons.image,
                color: Colors.grey,
              ),
              width: 60.0,
              height: 60.0,
            ),
            Container(
              margin: EdgeInsets.only(top: 8.0),
              child: Text(
                'No Image Captured',
                style: TextStyle(
                  color: Colors.grey,
                  fontSize: 16.0,
                ),
              ),
            ),
          ],
        ));
  }

  Widget capturedImageWidget(String imagePath) {
    return SizedBox.expand(
      child:Stack(
        children: <Widget>[
       Positioned(
         bottom: 20,
         child:Image.file(File(
        imagePath,
      ), fit:BoxFit.cover),),
          Opacity(
            opacity: 1,
            child:Container(
              width:MediaQuery.of(context).size.width ,
              color: Color.fromRGBO(39, 73, 34, 0.65),
                  child:Opacity(
                    opacity: 1,
                    child:Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment:  CrossAxisAlignment.center,
                children: <Widget>[
                  Image.asset(
                    'assets/images/scan1.png',
                    width: 70,
                    alignment: Alignment.center,
                  ),Padding(
                    padding: EdgeInsets.only(top:18),
                    child:Image.asset(
                    'assets/images/scan2.png',
                    width: 150,
                    alignment: Alignment.center,
                  ),),
            ],
            ),),
            )
          ),

    ]),);
  }

  Widget fabWidget() {
    return Positioned(
      bottom: 10.0,
      child:_imagePath != null
          ?  Container(width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(horizontal: 12),
        margin: new EdgeInsets.only(bottom: 12),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(25.0),
          child: RaisedButton(
            padding: new EdgeInsets.all(15),
            textColor: Color(0xFF273522),
            color: Color(0xFFFFFFFF),
            onPressed: faceDetect,
            child:
            Text('Далее',style: GoogleFonts.rubik(textStyle:TextStyle(fontSize: 20))),
          ),
        ),
      ): Container(width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(horizontal: 12),
        margin: new EdgeInsets.only(bottom: 12),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(25.0),
          child: RaisedButton(
            padding: new EdgeInsets.all(15),
            textColor: Colors.white,
            color: Color(0xFF273522),
            onPressed: openCamera,
            child:
            Text('Продолжить', style: GoogleFonts.rubik(textStyle:TextStyle(fontSize: 20))),
          ),
        ),
      )
    );
  }

  Future openCamera() async {
    availableCameras().then((cameras) async {
      final imagePath = await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => CameraPage(cameras),
        ),
      );
      setState(() {
        _imagePath = imagePath;
      });
    });
  }
  Future faceDetect() async {
    File imageFile = new File(_imagePath);
    List<int> imageBytes = imageFile.readAsBytesSync();
    String base64Image = base64Encode(imageBytes);
    http.post('https://16q6eimvi8.execute-api.us-east-2.amazonaws.com/prod/face',
        headers: {
      "Content-type": "application/json",
    }, body: json.encode({
      "face": base64Image,
    })).then((response) {
      print("Response status: ${response.statusCode}");
      print("Response body: ${response.body}");
    });

    String barcode = await scanner.scan();
    setState(() => this.barcode = barcode);

  }
}

