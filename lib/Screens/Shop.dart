import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:setea2/Components/CookiesBox.dart';
import 'package:setea2/setea_icons_icons.dart';
import '../Components/CircleItem.dart';
import '../Components/Teabox.dart';
import '../Components/Product.dart';
import 'package:setea2/setea2_icons.dart';
import '../Screens/Cart.dart';
import '../Components/TransitionAppBar.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:google_fonts/google_fonts.dart';

final String icon1 = 'assets/images/icon1.svg';

var textStyle = TextStyle(
    fontSize: 32.0,
    color: Colors.white,
    fontWeight: FontWeight.w600,
    fontFamily: 'Rubik');
var textStyle1 = TextStyle(
    fontSize: 24.0,
    color: Colors.grey[850],
    fontWeight: FontWeight.w200,
    fontFamily: 'Rubik');
var textStyle2 = TextStyle(
    fontSize: 20.0,
    color: Colors.black54,
    fontWeight: FontWeight.w100,
    fontFamily: 'Rubik');
var textStyle3 = TextStyle(
    fontSize: 18.0,
    color: Colors.green,
    fontWeight: FontWeight.w100,
    fontFamily: 'Rubik');
var textStyle5 = TextStyle(
    fontSize: 22.0,
    color: Colors.black,
    fontWeight: FontWeight.w500,
    fontFamily: 'Rubik');
var textStyle4 = TextStyle(
    fontSize: 26.0,
    color: Colors.green,
    fontWeight: FontWeight.w900,
    fontFamily: 'Rubik');
var textStyle6 = TextStyle(
    fontSize: 20.0,
    color: Colors.grey[400],
    fontWeight: FontWeight.normal,
    fontFamily: 'Rubik');
var textStyle7 = TextStyle(
    fontSize: 20.0,
    color: Colors.green,
    fontWeight: FontWeight.w200,
    fontFamily: 'Rubik');
var textStyle8 = TextStyle(
    fontSize: 20.0,
    color: Colors.black,
    fontWeight: FontWeight.w200,
    fontFamily: 'Rubik');


class Shop extends StatefulWidget {
  @override
  createState() => new ShopState();
}

class ShopState extends State<Shop> {

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        body: CustomScrollView(
        slivers: <Widget>[
          SliverPersistentHeader(
            delegate: MySliverAppBar(expandedHeight: 260),
            pinned: true,
          ),
          SliverList(
            delegate: SliverChildListDelegate(
            [
              Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only( left: 8.0, bottom: 55.0),
              ),
              Container(
                height: 170.0,
                child: ListView(
                  padding: const EdgeInsets.all(10),
                  scrollDirection: Axis.horizontal,
                  children: <Widget>[
                    CircleItem('Праздник', 'assets/images/cup3.png'),
                    CircleItem('Чай', 'assets/images/cup2.png'),
                    CircleItem('Сэндвичи', 'assets/images/cup4.png'),
                    CircleItem('Десерт', 'assets/images/cup5.png'),
                    CircleItem('Разное', 'assets/images/cup5.png'),
                  ],
                ),
              ),
              Container(

                child:Column(
    children: <Widget>[
      Product(),
      Product(),
      Product(),
      Product(),
      Product(),
      Product(),
    ],
    )


              ),
            ],
          ),

        ],

    ),
          ),
    ]),
    );
  }


}
class MySliverAppBar extends SliverPersistentHeaderDelegate {
  final double expandedHeight;

  MySliverAppBar({@required this.expandedHeight});

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Stack(
      fit: StackFit.expand,
      overflow: Overflow.visible,
      children: [
        AppBar(
          bottomOpacity: 0,
          elevation:0,
          centerTitle: true,
      actions: <Widget>[
        IconButton(
            icon: const Icon(Setea2.vector__7_, color: Colors.white, size: 25),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Cart()),
              );
            }
        ),
      ],
          title: Image.asset('assets/images/setea.png', width: 90, alignment: Alignment.center,),
          flexibleSpace: Image(
            image: AssetImage('assets/images/profback.png'),
            fit: BoxFit.cover,
          ),
        ),
        Positioned(
            top: expandedHeight*0.56 - shrinkOffset*2.19,
            child: Container(
              child: Opacity(
                opacity: (1 - shrinkOffset / expandedHeight),
                child: SizedBox(
                  height: expandedHeight/1.5,
                  width: MediaQuery.of(context).size.width *1,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                  Teabox(),
                    ],
                  ),
                ),
              ),
            )
         ),
        Positioned(
            top: expandedHeight*0.3 - shrinkOffset*2.19,
              left: 20,
    child: Opacity(
    opacity: (1 - shrinkOffset / expandedHeight),
            child:Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: <Widget>[
      Text('Магазин', style:GoogleFonts.rubik(textStyle: textStyle,)),
    ],
    ),),
        ),
        Positioned(
          top: expandedHeight*0.349 - shrinkOffset*2.19,
          right: 20,
          child: Opacity(
            opacity: (1 - shrinkOffset / expandedHeight),
            child:Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
               Icon(AntDesign.search1, color:Colors.white),
              ],
            ),),
        ),
      ],
    );
  }

  @override
  double get maxExtent => expandedHeight;

  @override
  double get minExtent => kToolbarHeight+30;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) => true;
}