import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:setea2/Screens/Settings.dart';
import 'package:setea2/setea_icons_icons.dart';
import '../Components/CircleItem.dart';
import '../Components/Profilehead.dart';
import '../Components/Teabox.dart';
import '../Components/Textwlink.dart';
import '../Components/History.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:google_fonts/google_fonts.dart';

var textStyle = TextStyle(fontSize: 32.0, color: Colors.black, fontWeight:
FontWeight.w600,fontFamily: 'Rubik' );
var textStyle1 = TextStyle(fontSize: 24.0, color: Colors.grey[850], fontWeight:
FontWeight.w200, fontFamily: 'Rubik');
var textStyle2 = TextStyle(fontSize: 20.0, color: Colors.black54, fontWeight:
FontWeight.w100, fontFamily: 'Rubik');
var textStyle3 = TextStyle(fontSize: 16.0, color: Colors.green, fontWeight:
FontWeight.w100, fontFamily: 'Rubik');
var textStyle5 = TextStyle(fontSize: 22.0, color: Colors.black, fontWeight:
FontWeight.w500, fontFamily: 'Rubik');
var textStyle4 = TextStyle(fontSize: 26.0, color: Colors.green, fontWeight:
FontWeight.w900, fontFamily: 'Rubik');
var textStyle6 = TextStyle(fontSize: 16.0, color: Color(0xFFEAEAEA), fontWeight:
FontWeight.normal, fontFamily: 'Rubik');
var textStyle7 = TextStyle(fontSize: 20.0, color: Colors.green, fontWeight:
FontWeight.w200, fontFamily: 'Rubik');
var textStyle8 = TextStyle(fontSize: 20.0, color: Colors.black, fontWeight:
FontWeight.w200, fontFamily: 'Rubik');

class Profile extends StatefulWidget {
  @override
  createState() => new ProfileState();
}


class ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:AppBar(
        bottomOpacity: 0,
          elevation:0,
        backgroundColor: Color(0xFF273522),
        leading:IconButton(
          icon: const Icon(SimpleLineIcons.menu, color: Colors.white, size: 30),
          onPressed: () {
            Scaffold.of(context).openDrawer();
          },
        ),
        title: Row(
            mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
          Image.asset('assets/images/setea.png', width: 90, alignment: Alignment.center,),]),
          actions: <Widget>[
      IconButton(
      icon: const Icon(SeteaIcons.settings, color: Colors.white, size: 25),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Settings()),
            );
          }
    ),
   ],
      ),
      body:Container(
          child:ListView(
          children: <Widget>[
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/images/profback.png", ),
                  fit: BoxFit.cover,
                  alignment: Alignment.topCenter
              ),
            ),
            child:Column(
            children: <Widget>[
            Profilehead('http://all4desktop.com/data_images/original/4242435-face.jpg','CurrentUser'),
          Teabox(),]),),
            History(),
            Container(
              margin: new EdgeInsets.symmetric(horizontal: 16.0,),
              decoration: const BoxDecoration(
              border: Border(
                bottom: BorderSide(width: 1.0, color: Color(0xFFEAEAEA)),
              ),
            ),),
            Textwlink('Мне понравилось'),
            Textwlink('Политика конфиденциальности'),
            Textwlink('Пользовательское соглашение'),
            Container(
              alignment: Alignment.center,
              child: InkWell(onTap: () {},
                borderRadius: new BorderRadius.circular(20.0),
                  child:Container(
                    padding: new EdgeInsets.symmetric(vertical:18, horizontal: 18),
              child: Text('Выйти из приложения',  style:GoogleFonts.rubik(textStyle: textStyle6)),),
              )
            ),
            Container(

              margin: new EdgeInsets.only(bottom:50)
            )
          ],
    ),

      ),
    );

       }}