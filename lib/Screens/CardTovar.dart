import 'package:flutter/material.dart';
import '../Screens/Shop.dart';
import '../setea_icons_icons.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import '../Components/Bigteacup.dart';
import 'Cart.dart';

class CardTovar extends StatefulWidget {
  @override
  createState() => new CardTovarState();
}

class CardTovarState extends State<CardTovar> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          bottomOpacity: 0,
          elevation: 0,
          backgroundColor: Color(0xFF273522),
          leading: IconButton(
            icon: const Icon(AntDesign.left, color: Colors.white, size: 30),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          title: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Image.asset(
                  'assets/images/seteagreen.png',
                  width: 90,
                  alignment: Alignment.center,
                ),
              ]),
          actions: <Widget>[
            IconButton(
                padding: EdgeInsets.only(right: 12),
                icon: const Icon(Feather.shopping_cart,
                    color: Colors.white, size: 20),
                onPressed: () {

                }),
          ],
        ),
        body: Container(
          color: Colors.white,
          child: ListView(
            scrollDirection: Axis.vertical,
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  HeadCard(),
                  ContentCard(),
                  Stakan(),
                  AddTovar(),
                ],
              ),
            ],
          ),
        ));
  }
}

class HeadCard extends StatefulWidget {
  @override
  createState() => new HeadCardState();
}

class HeadCardState extends State<HeadCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Color(0xFF273522),
        image: DecorationImage(
          image: AssetImage("assets/images/Intersect.png"),
          fit: BoxFit.fitWidth,
        ),
      ),
      padding: EdgeInsets.symmetric(horizontal: 18),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Toxic Tea',
            style: TextStyle(
                color: Colors.white, fontSize: 34, fontWeight: FontWeight.w700),
          ),
          Container(
//                  height: 200,
            margin: EdgeInsets.symmetric(vertical: 18),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Цена',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w100,
                      fontSize: 16),
                ),
                Text(
                  '\$5',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w700,
                      fontSize: 24),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  'Выберете размер',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w100,
                      fontSize: 16),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(right: 12),
                      padding: EdgeInsets.all(0),
                      decoration: new BoxDecoration(
                          shape: BoxShape.circle, color: Colors.white),
                      child: Icon(
                        SeteaIcons.cupm,
                        color: Color(0xFF273522),
                        size: 40,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(right: 12),
                      padding: EdgeInsets.all(0),
                      decoration: new BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(color: Colors.white)),
                      child: Icon(
                        SeteaIcons.cupm,
                        color: Colors.white,
                        size: 40,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(0),
                      decoration: new BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(color: Colors.white)),
                      child: Icon(
                        SeteaIcons.cupl,
                        color: Colors.white,
                        size: 40,
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class ContentCard extends StatefulWidget {
  @override
  createState() => new ContentCardState();
}

class ContentCardState extends State<ContentCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xFF273522),
      child: ClipRRect(
        borderRadius: new BorderRadius.only(
          topLeft: const Radius.circular(20),
          topRight: const Radius.circular(20),
          bottomLeft: const Radius.circular(15.0),
          bottomRight: const Radius.circular(15.0),
        ),
        child:
        Container(
            padding: EdgeInsets.symmetric(
              vertical: 24,
              horizontal: 18,
            ),
            width: MediaQuery.of(context).size.width,
            color: Colors.white,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Взбодритесь!',
                  style: TextStyle(
                      color: Color(0xFF303030),
                      fontFamily: 'Rubik',
                      fontSize: 24,
                      fontWeight: FontWeight.w700),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'Равным образом реализация намеченных плановых заданий играет важную роль в формир.',
                  style: TextStyle(
                      color: Color(0xFF787878),
                      fontFamily: 'Rubik',
                      fontSize: 14,
                      fontWeight: FontWeight.w200),
                ),
                SizedBox(
                  height: 18,
                ),
                FlatButton(
                  shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(18.0),
                  ),
                  color: Color(0xFF273522),
                  textColor: Colors.white,
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Cart()),
                    );
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("assets/images/Intersect.png"),
                        fit: BoxFit.fitHeight,
                      ),),
                      width: 350,
                      height: 50,
                      child: Center(
                        child: Text(
                          "Купить",
                          style: TextStyle(
                            fontSize: 18,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      )),
                ),
                SizedBox(
                  height: 18,
                ),
                Container(
                  height: 240.0,
                  child: ListView(
                      padding: const EdgeInsets.all(10),
                      scrollDirection: Axis.horizontal,
                      children: <Widget>[
                        Bigteacup('Toxic Tea', '5', 'assets/images/cup5.png'),
                        Bigteacup('Toxic Tea', '5', 'assets/images/supercup.png'),
                        Bigteacup('Toxic Tea', '5', 'assets/images/supercup.png'),
                      ]),
                ),
              ],
            )
        ),
      ),
    );
  }
}

class Stakan extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 0,
      child: Stack(
        fit: StackFit.expand,
        overflow: Overflow.visible,
        children: <Widget>[
          Positioned(
              left: MediaQuery.of(context).size.width * 0.5,
              top: -600,
              child: SizedBox(
                child: Image.asset(
                  'assets/images/supercup.png',
                  width: 130,
                ),
              ))
        ],
      ),
    );
  }
}


class AddTovar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xFF273522),
      height: 62,
      padding: EdgeInsets.symmetric(horizontal: 12),
      child:
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Container(
            width: 80,
            child: Stack(
              children: <Widget>[
                Container(
                  width: 40,
                  decoration: BoxDecoration(
                    color: Color(0xFF096F2F),
                    shape: BoxShape.circle,
                  ),
                ),
                Positioned(
                  left: 7,
                  top: 3,
                  child: Image.asset('assets/images/supercup.png', width: 28,),
                ),
                Positioned(
                    left: 25,
                    top: -1,
                    child: Container(
                        padding: EdgeInsets.all(3),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.circle,
                        ),
                        child: Text(
                          'S',
                          style: TextStyle(
                              color: Color(0xFF303030), fontSize: 18),
                        )))
              ],
            ),
          ),
          Text('Товар добавлен в корзину',style:TextStyle(fontSize: 18, color: Colors.white)),
          Icon(Ionicons.ios_arrow_round_forward, color: Colors.white, size: 35,)
        ],
      ),
    );
  }
}
