import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

import 'package:flutter_vector_icons/flutter_vector_icons.dart';

var textStyle = TextStyle(
    fontSize: 32.0,
    color: Colors.black,
    fontWeight: FontWeight.w600,
    fontFamily: 'Rubik');
var textStyle1 = TextStyle(
    fontSize: 24.0,
    color: Colors.grey[850],
    fontWeight: FontWeight.w200,
    fontFamily: 'Rubik');
var textStyle2 = TextStyle(
    fontSize: 22.0,
    color: Colors.black54,
    fontWeight: FontWeight.w100,
    fontFamily: 'Rubik');
var textStyle3 = TextStyle(
    fontSize: 16.0,
    color: Colors.green,
    fontWeight: FontWeight.w100,
    fontFamily: 'Rubik');
var textStyle5 = TextStyle(
    fontSize: 22.0,
    color: Colors.black,
    fontWeight: FontWeight.w500,
    fontFamily: 'Rubik');
var textStyle4 = TextStyle(
    fontSize: 26.0,
    color: Colors.green,
    fontWeight: FontWeight.w900,
    fontFamily: 'Rubik');
var textStyle6 = TextStyle(
    fontSize: 16.0,
    color: Color(0xFFEAEAEA),
    fontWeight: FontWeight.normal,
    fontFamily: 'Rubik');
var textStyle7 = TextStyle(
    fontSize: 20.0,
    color: Colors.green,
    fontWeight: FontWeight.w200,
    fontFamily: 'Rubik');
var textStyle8 = TextStyle(
    fontSize: 20.0,
    color: Colors.black,
    fontWeight: FontWeight.w200,
    fontFamily: 'Rubik');

class Filter extends StatefulWidget {
  @override
  createState() => new FilterState();
}

final myController = TextEditingController();

TextEditingController nameController = TextEditingController();
TextEditingController secondnameController = TextEditingController();
TextEditingController phoneController = TextEditingController();
TextEditingController emailController = TextEditingController();
TextEditingController cityController = TextEditingController();

class FilterState extends State<Filter> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
    appBar: AppBar(
    bottomOpacity: 0,
    elevation: 0,
    backgroundColor: Color(0xFFFFFFFF),
    leading:IconButton(
    icon: const Icon(AntDesign.left, color: Color(0xFF273522), size: 30),
    onPressed: () {
    Navigator.pop(context);
    },
    ),
    actions: <Widget>[
    IconButton(
    icon: const Icon(Feather.shopping_cart,
    color: Color(0xFF273522), size: 25),
    onPressed: () {
    }),
    ],
    ),
      body: Container(
          decoration: BoxDecoration(color: Colors.white),
          child: ListView(
            children: <Widget>[
             Container(
                margin: new EdgeInsets.only(left: 16.0, right: 16, top: 20, bottom: 12),
                decoration: const BoxDecoration(
                ),
             child:Text('Фильтр', style:textStyle),
             ),
              Container(
                margin: new EdgeInsets.only(left: 16.0, right: 16,  bottom: 12),
                decoration: const BoxDecoration(
                ),
                child:Text('Цена', style:textStyle2),
              ),
              Container(
               padding: EdgeInsets.symmetric(horizontal: 12),
                child: Theme(
              data: new ThemeData(
              primaryColor: Colors.green,
              primaryColorDark: Colors.green,
              ),
                child:Row(
                  children: <Widget>[
                    Container(

                      child:Flexible(
                  child: TextField(
                    controller: nameController,
                    decoration: InputDecoration(
                        hoverColor: Colors.green,
                        labelText: 'От',
                        fillColor: Color(0xFFF8F8F8),
                        filled: true,
                        focusColor: Colors.green,
                        border: OutlineInputBorder(),
                        hintStyle: textStyle5),
                  ),
                ),),Container(padding: EdgeInsets.symmetric(horizontal: 5),),
    Flexible(                  child: TextField(
                    controller: secondnameController,
                    decoration: InputDecoration(
                      fillColor: Color(0xFFF8F8F8),
                      border: OutlineInputBorder(),
                      hoverColor: Colors.green,
                      labelText: 'До',
                      filled:true,
                    ),
                  ),
                ),
              ]),),),
              Container(
                margin: new EdgeInsets.only(left: 16.0, right: 16,top:12,  bottom: 12),
                decoration: const BoxDecoration(
                ),
                child:Text('Размер', style:textStyle2),
              ),
              Row(
                children: <Widget>[
                  Container(
                    padding: new EdgeInsets.only(left: 6, right:2),
                    margin: new EdgeInsets.only(bottom: 20),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10.0),
                      child: RaisedButton(
                        padding: new EdgeInsets.symmetric(horizontal: 35, vertical:15 ),
                        textColor: Colors.black,
                        color: Colors.grey,
                        onPressed: () {},
                        child:
                        const Text('Small', style: TextStyle(fontSize: 20)),
                      ),
                    ),
                  ),Container(
                    padding: new EdgeInsets.symmetric(horizontal: 4),
                    margin: new EdgeInsets.only(bottom: 20),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10.0),
                      child: RaisedButton(
                        padding: new EdgeInsets.symmetric(horizontal: 35, vertical:15 ),
                        textColor: Colors.white,
                        color: Colors.green,
                        onPressed: () {},
                        child:
                        const Text('Medium', style: TextStyle(fontSize: 20)),
                      ),
                    ),
                  ),Container(
                    padding: new EdgeInsets.symmetric(horizontal: 2),
                    margin: new EdgeInsets.only(bottom: 20),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10.0),
                      child: RaisedButton(
                        padding: new EdgeInsets.symmetric(horizontal: 35, vertical:15 ),
                        textColor: Colors.black,
                        color: Colors.grey,
                        onPressed: () {},
                        child:
                        const Text('Large', style: TextStyle(fontSize: 20)),
                      ),
                    ),
                  ),
                ],
              ),
              Container(
                margin: new EdgeInsets.only(left: 16.0, right: 16,  bottom: 12),
                decoration: const BoxDecoration(
                ),
                child:Text('Сортировать', style:textStyle2),
              ),
              Container(

                padding: EdgeInsets.symmetric(horizontal: 10),
                child:Column(

                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width,
                    padding: new EdgeInsets.symmetric(horizontal: 2),
                    margin: new EdgeInsets.only(bottom: 10),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10.0),
                      child: RaisedButton(
                        padding: new EdgeInsets.symmetric(horizontal: 35, vertical:15 ),
                        textColor: Colors.black,
                        color: Colors.grey,
                        onPressed: () {},
                        child:
                        const Text('По популярности', style: TextStyle(fontSize: 20)),
                      ),
                    ),
                  ),Container(
                    width: MediaQuery.of(context).size.width,
                    padding: new EdgeInsets.symmetric(horizontal: 2),
                    margin: new EdgeInsets.only(bottom: 10),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10.0),
                      child: RaisedButton(
                        padding: new EdgeInsets.symmetric(horizontal: 35, vertical:15 ),
                        textColor: Colors.black,
                        color: Colors.grey,
                        onPressed: () {},
                        child:
                        const Text('По возрастанию цены', style: TextStyle(fontSize: 20)),
                      ),
                    ),
                  ),Container(
                    width: MediaQuery.of(context).size.width,
                    padding: new EdgeInsets.symmetric(horizontal: 2),
                    margin: new EdgeInsets.only(bottom: 60),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10.0),
                      child: RaisedButton(
                        padding: new EdgeInsets.symmetric(horizontal: 35, vertical:15 ),
                        textColor: Colors.black,
                        color: Colors.grey,
                        onPressed: () {},
                        child:
                        const Text('По убыванию цены ', style: TextStyle(fontSize: 20)),
                      ),
                    ),
                  ),
                ],),
              ),
              Container(
                padding: new EdgeInsets.symmetric(horizontal: 16),
                margin: new EdgeInsets.only(bottom: 50),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(25.0),
                  child: RaisedButton(
                    padding: new EdgeInsets.all(15),
                    textColor: Colors.white,
                    color: Colors.green,
                    onPressed: () {},
                    child:
                    const Text('Сохранить', style: TextStyle(fontSize: 20)),
                  ),
                ),
              ),
            ],
          ),),
    );
  }
}
