import 'package:flutter/material.dart';
import '../setea_icons_icons.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import '../main.dart';
import '../Components/BottomNavyBar.dart';

class Music extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Material(
        color: Colors.white,
        child: CustomScrollView(
          slivers: [
            SliverPersistentHeader(
              delegate: MySliverAppBar(expandedHeight: 250),
              pinned: true,
            ),
            SliverList(
              delegate: SliverChildListDelegate([
                SizedBox(
                  height: 50,
                ),
                Text(
                  'Энергичное настроение',
                  style: TextStyle(fontSize: 24,fontFamily: 'Rubik',fontWeight: FontWeight.w600,color: Color(0xFF303030)),
                  textAlign: TextAlign.center,
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 10),
                  margin: EdgeInsets.only(left: 20,right: 20),
                  decoration: const BoxDecoration(
                    border: Border(
                      bottom: BorderSide(width: 2, color: Color(0xFFEAEAEA)),
                    ),
                  ),
                  child: Text(
                    'Разнообразный и богатый опыт постоянное информа-пропагандистское.',
                    style: TextStyle(fontSize: 16, color: Color(0xFF787878), fontWeight:
                    FontWeight.w500,fontFamily: 'Rubik'),
                    textAlign: TextAlign.center,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 20,right: 20),
                  decoration: const BoxDecoration(
                    border: Border(
                      bottom: BorderSide(width: 2, color: Color(0xFFEAEAEA)),
                    ),
                  ),
                  child: ListTile(
                    leading: ClipRRect(
                      borderRadius: BorderRadius.circular(5),
                      child: Image.asset('assets/images/song.jpg', height: 40,),
                    ),
                    title: Text('One-line with leading widget',
                      style: TextStyle(color: Color(0xFF303030), fontSize: 18,fontFamily: 'Rubik', fontWeight: FontWeight.w700),),
                    subtitle: Text('Here is a second line'),
                    trailing: Icon(SeteaIcons.like, color: Color(0xFF1CAA53),),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 20,right: 20),
                  decoration: const BoxDecoration(
                    border: Border(
                      bottom: BorderSide(width: 2, color: Color(0xFFEAEAEA)),
                    ),
                  ),
                  child: ListTile(
                    leading: ClipRRect(
                      borderRadius: BorderRadius.circular(5),
                      child: Image.asset('assets/images/song.jpg', height: 40,),
                    ),
                    title: Text('One-line with leading widget'),
                    subtitle: Text('Here is a second line'),
                    trailing: Icon(SeteaIcons.like, color: Color(0xFF1CAA53),),
                  ),
                ),

                Container(
                  margin: EdgeInsets.only(left: 20,right: 20),
                  decoration: const BoxDecoration(
                    border: Border(
                      bottom: BorderSide(width: 2, color: Color(0xFFEAEAEA)),
                    ),
                  ),
                  child: ListTile(
                    leading: ClipRRect(
                      borderRadius: BorderRadius.circular(5),
                      child: Image.asset('assets/images/song.jpg', height: 40,),
                    ),
                    title: Text('One-line with leading widget'),
                    subtitle: Text('Here is a second line'),
                    trailing: Icon(SeteaIcons.like, color: Color(0xFF1CAA53),),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 20,right: 20),
                  decoration: const BoxDecoration(
                    border: Border(
                      bottom: BorderSide(width: 2, color: Color(0xFFEAEAEA)),
                    ),
                  ),
                  child: ListTile(
                    leading: ClipRRect(
                      borderRadius: BorderRadius.circular(5),
                      child: Image.asset('assets/images/song.jpg', height: 40,),
                    ),
                    title: Text('One-line with leading widget'),
                    subtitle: Text('Here is a second line'),
                    trailing: Icon(SeteaIcons.like, color: Color(0xFF1CAA53),),
                  ),
                ),

                Container(
                  margin: EdgeInsets.only(left: 20,right: 20),
                  decoration: const BoxDecoration(
                    border: Border(
                      bottom: BorderSide(width: 2, color: Color(0xFFEAEAEA)),
                    ),
                  ),
                  child: ListTile(
                    leading: ClipRRect(
                      borderRadius: BorderRadius.circular(5),
                      child: Image.asset('assets/images/song.jpg', height: 40,),
                    ),
                    title: Text('One-line with leading widget'),
                    subtitle: Text('Here is a second line'),
                    trailing: Icon(SeteaIcons.like, color: Color(0xFF1CAA53),),
                  ),
                ),

                Container(
                  margin: EdgeInsets.only(left: 20,right: 20),
                  decoration: const BoxDecoration(
                    border: Border(
                      bottom: BorderSide(width: 2, color: Color(0xFFEAEAEA)),
                    ),
                  ),
                  child: ListTile(
                    leading: ClipRRect(
                      borderRadius: BorderRadius.circular(5),
                      child: Image.asset('assets/images/song1.png', height: 40,),
                    ),
                    title: Text('One-line with leading widget'),
                    subtitle: Text('Here is a second line'),
                    trailing: Icon(SeteaIcons.like, color: Color(0xFFE9E9EA),),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 20,right: 20),
                  decoration: const BoxDecoration(
                    border: Border(
                      bottom: BorderSide(width: 2, color: Color(0xFFEAEAEA)),
                    ),
                  ),
                  child: ListTile(
                    leading: ClipRRect(
                      borderRadius: BorderRadius.circular(5),
                      child: Image.asset('assets/images/song.jpg', height: 40,),
                    ),
                    title: Text('One-line with leading widget'),
                    subtitle: Text('Here is a second line'),
                    trailing: Icon(SeteaIcons.like, color: Color(0xFF1CAA53),),
                  ),
                ),

                Container(
                  margin: EdgeInsets.only(left: 20,right: 20),
                  decoration: const BoxDecoration(
                    border: Border(
                      bottom: BorderSide(width: 2, color: Color(0xFFEAEAEA)),
                    ),
                  ),
                  child: ListTile(
                    leading: ClipRRect(
                      borderRadius: BorderRadius.circular(5),
                      child: Image.asset('assets/images/song1.png', height: 40,),
                    ),
                    title: Text('One-line with leading widget'),
                    subtitle: Text('Here is a second line'),
                    trailing: Icon(SeteaIcons.like, color: Color(0xFFE9E9EA),),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 20,right: 20),
                  decoration: const BoxDecoration(
                    border: Border(
                      bottom: BorderSide(width: 2, color: Color(0xFFEAEAEA)),
                    ),
                  ),
                  child: ListTile(
                    leading: ClipRRect(
                      borderRadius: BorderRadius.circular(5),
                      child: Image.asset('assets/images/song.jpg', height: 40,),
                    ),
                    title: Text('One-line with leading widget'),
                    subtitle: Text('Here is a second line'),
                    trailing: Icon(SeteaIcons.like, color: Color(0xFF1CAA53),),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 20,right: 20),
                  decoration: const BoxDecoration(
                    border: Border(
                      bottom: BorderSide(width: 2, color: Color(0xFFEAEAEA)),
                    ),
                  ),
                  child: ListTile(
                    leading: ClipRRect(
                      borderRadius: BorderRadius.circular(5),
                      child: Image.asset('assets/images/song.jpg', height: 40,),
                    ),
                    title: Text('One-line with leading widget'),
                    subtitle: Text('Here is a second line'),
                    trailing: Icon(SeteaIcons.like, color: Color(0xFF1CAA53),),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 20,right: 20),
                  decoration: const BoxDecoration(
                    border: Border(
                      bottom: BorderSide(width: 2, color: Color(0xFFEAEAEA)),
                    ),
                  ),
                  child: ListTile(
                    leading: ClipRRect(
                      borderRadius: BorderRadius.circular(5),
                      child: Image.asset('assets/images/song.jpg', height: 40,),
                    ),
                    title: Text('One-line with leading widget'),
                    subtitle: Text('Here is a second line'),
                    trailing: Icon(SeteaIcons.like, color: Color(0xFF1CAA53),),
                  ),
                ),
                Row(
                  children: <Widget>[
                    Flexible(child: WhereMusic(),),
                    Flexible(child: WhereMusic(imageurl: 'assets/images/spotify.png', executor: 'Слушать в Spotify',)),
                  ],
                )
              ]),
            )
          ],
        ),
      ),
    );
  }
}

class MySliverAppBar extends SliverPersistentHeaderDelegate {
  final double expandedHeight;

  MySliverAppBar({@required this.expandedHeight});

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Stack(
      fit: StackFit.expand,
      overflow: Overflow.visible,
      children: [
        AppBar(
          bottomOpacity: 0,
          elevation:0,
          centerTitle: true,
          leading: IconButton(
            icon: const Icon(AntDesign.left, color: Colors.white, size: 30),
            onPressed:(){Navigator.pop(context);},
          ),
          title: Image.asset('assets/images/setea.png', width: 90, alignment: Alignment.center,),
          flexibleSpace: Image(
            image: AssetImage('assets/images/backgroundmusic.png'),
            fit: BoxFit.cover,
          ),
        ),
        Positioned(
          top: expandedHeight*0.8 - shrinkOffset*1.2,
          left: MediaQuery.of(context).size.width*0.05,
          child: Material(
            borderRadius: BorderRadius.circular(10),
            elevation: 5,
            child: Opacity(
              opacity: (1 - shrinkOffset / expandedHeight),
              child: SizedBox(
                height: expandedHeight/3,
                width: MediaQuery.of(context).size.width *0.9,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    ListTile(
                      leading: ClipRRect(
                        borderRadius: BorderRadius.circular(5),
                        child: Image.asset('assets/images/song.jpg', height: 60,),
                      ),
                      title: Text('One-line with leading widget'),
                      subtitle: Text('Here is a second line'),
                      trailing: Icon(SeteaIcons.play, color: Color(0xFF1CAA53),),
                    ),
                  ],
                ),
              ),
            ),
          )
        ),
      ],
    );
  }

  @override
  double get maxExtent => expandedHeight;

  @override
  double get minExtent => kToolbarHeight+30;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) => true;
}


class WhereMusic extends StatelessWidget {

  String _executor;
  String _imageurl;

  WhereMusic({String imageurl = 'assets/images/yandex.png',  String executor = 'Слушать в ЯндексМузыке',}){
    _imageurl = imageurl;
    _executor = executor;
  }

  @override
  Widget build(BuildContext context) {
    return
      Container(
        margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Color(0xFFF8F8F8)
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(5),
              margin: EdgeInsets.only(right: 10),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10)
              ),
              child: Image.asset(_imageurl),
            ),
            Flexible(
              child: Text(
                  _executor,
                  style: TextStyle(fontSize: 14, color: Color(0xFF303030), fontWeight:
                  FontWeight.w500,fontFamily: 'Rubik')
              ),
            )
          ],
      )
    );
  }
}