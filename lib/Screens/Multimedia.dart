import 'package:flutter/material.dart';
import '../Components/CookiesBox.dart';
import './Articles.dart';

var textStyle = TextStyle(
    fontSize: 32.0,
    color: Colors.black,
    fontWeight: FontWeight.w600,
    fontFamily: 'Rubik');
var textStyle1 = TextStyle(
    fontSize: 24.0,
    color: Colors.grey[850],
    fontWeight: FontWeight.w200,
    fontFamily: 'Rubik');

class Multimedia extends StatefulWidget {
  @override
  createState() => new MultimediaState();
}

//class MultimediaState extends State<Multimedia> {
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      body: Material(
//          color: Colors.white,
//          child: Stack(
//            children: <Widget>[
//              CustomScrollView(
//                slivers: [
//                  SliverPersistentHeader(
//                    delegate: MySliverAppBar(expandedHeight: 250),
//                    pinned: true,
//                  ),
//                  SliverList(
//                    delegate: SliverChildListDelegate([
//                      SizedBox(
//                        height: 30,
//                      ),
//                      Text(
//                        'Энергичное настроение',
//                        style: TextStyle(
//                            fontSize: 24,
//                            fontFamily: 'Rubik',
//                            fontWeight: FontWeight.w600,
//                            color: Color(0xFF303030)),
//                        textAlign: TextAlign.center,
//                      ),
//                      Container(
//                        padding: EdgeInsets.only(bottom: 10),
//                        margin: EdgeInsets.only(left: 20, right: 20),
//                        decoration: const BoxDecoration(
//                          border: Border(
//                            bottom:
//                                BorderSide(width: 2, color: Color(0xFFEAEAEA)),
//                          ),
//                        ),
//                        child: Text(
//                          'Разнообразный и богатый опыт постоянное информа-пропагандистское.',
//                          style: TextStyle(
//                              fontSize: 16,
//                              color: Color(0xFF787878),
//                              fontWeight: FontWeight.w500,
//                              fontFamily: 'Rubik'),
//                          textAlign: TextAlign.center,
//                        ),
//                      ),
//                      Container(
//                        margin: EdgeInsets.only(left: 20, right: 20),
//                        decoration: const BoxDecoration(
//                          border: Border(
//                            bottom:
//                                BorderSide(width: 2, color: Color(0xFFEAEAEA)),
//                          ),
//                        ),
//                        child: ListTile(
//                          leading: ClipRRect(
//                            borderRadius: BorderRadius.circular(5),
//                            child: Image.asset(
//                              'assets/images/song.jpg',
//                              height: 40,
//                            ),
//                          ),
//                          title: Text(
//                            'One-line with leading widget',
//                            style: TextStyle(
//                                color: Color(0xFF303030),
//                                fontSize: 18,
//                                fontFamily: 'Rubik',
//                                fontWeight: FontWeight.w700),
//                          ),
//                          subtitle: Text('Here is a second line'),
//                          trailing: Icon(SeteaIcons.play,
//                              color: Color(0xFFE9E9EA), size: 20),
//                        ),
//                      ),
//                      Container(
//                        margin: EdgeInsets.only(left: 20, right: 20),
//                        decoration: const BoxDecoration(
//                          border: Border(
//                            bottom:
//                                BorderSide(width: 2, color: Color(0xFFEAEAEA)),
//                          ),
//                        ),
//                        child: ListTile(
//                          leading: ClipRRect(
//                            borderRadius: BorderRadius.circular(5),
//                            child: Image.asset(
//                              'assets/images/song.jpg',
//                              height: 40,
//                            ),
//                          ),
//                          title: Text('One-line with leading widget'),
//                          subtitle: Text('Here is a second line'),
//                          trailing: Icon(SeteaIcons.play,
//                              color: Color(0xFFE9E9EA), size: 20),
//                        ),
//                      ),
//                      Container(
//                        margin: EdgeInsets.only(left: 20, right: 20),
//                        decoration: const BoxDecoration(
//                          border: Border(
//                            bottom:
//                                BorderSide(width: 2, color: Color(0xFFEAEAEA)),
//                          ),
//                        ),
//                        child: ListTile(
//                          leading: ClipRRect(
//                            borderRadius: BorderRadius.circular(5),
//                            child: Image.asset(
//                              'assets/images/song.jpg',
//                              height: 40,
//                            ),
//                          ),
//                          title: Text('One-line with leading widget'),
//                          subtitle: Text('Here is a second line'),
//                          trailing: Icon(SeteaIcons.play,
//                              color: Color(0xFFE9E9EA), size: 20),
//                        ),
//                      ),
//                      Container(
//                        margin: EdgeInsets.only(left: 20, right: 20),
//                        decoration: const BoxDecoration(
//                          border: Border(
//                            bottom:
//                                BorderSide(width: 2, color: Color(0xFFEAEAEA)),
//                          ),
//                        ),
//                        child: ListTile(
//                          leading: ClipRRect(
//                            borderRadius: BorderRadius.circular(5),
//                            child: Image.asset(
//                              'assets/images/song.jpg',
//                              height: 40,
//                            ),
//                          ),
//                          title: Text('One-line with leading widget'),
//                          subtitle: Text('Here is a second line'),
//                          trailing: Icon(SeteaIcons.play,
//                              color: Color(0xFFE9E9EA), size: 20),
//                        ),
//                      ),
//                      Container(
//                        margin: EdgeInsets.only(left: 20, right: 20),
//                        decoration: const BoxDecoration(
//                          border: Border(
//                            bottom:
//                                BorderSide(width: 2, color: Color(0xFFEAEAEA)),
//                          ),
//                        ),
//                        child: ListTile(
//                          leading: ClipRRect(
//                            borderRadius: BorderRadius.circular(5),
//                            child: Image.asset(
//                              'assets/images/song.jpg',
//                              height: 40,
//                            ),
//                          ),
//                          title: Text('One-line with leading widget'),
//                          subtitle: Text('Here is a second line'),
//                          trailing: Icon(SeteaIcons.play,
//                              color: Color(0xFFE9E9EA), size: 20),
//                        ),
//                      ),
//                      Container(
//                        margin: EdgeInsets.only(left: 20, right: 20),
//                        decoration: const BoxDecoration(
//                          border: Border(
//                            bottom:
//                                BorderSide(width: 2, color: Color(0xFFEAEAEA)),
//                          ),
//                        ),
//                        child: ListTile(
//                          leading: ClipRRect(
//                            borderRadius: BorderRadius.circular(5),
//                            child: Image.asset(
//                              'assets/images/song1.png',
//                              height: 40,
//                            ),
//                          ),
//                          title: Text('One-line with leading widget'),
//                          subtitle: Text('Here is a second line'),
//                          trailing: Icon(SeteaIcons.play,
//                              color: Color(0xFFE9E9EA), size: 20),
//                        ),
//                      ),
//                      Container(
//                        margin: EdgeInsets.only(left: 20, right: 20),
//                        decoration: const BoxDecoration(
//                          border: Border(
//                            bottom:
//                                BorderSide(width: 2, color: Color(0xFFEAEAEA)),
//                          ),
//                        ),
//                        child: ListTile(
//                          leading: ClipRRect(
//                            borderRadius: BorderRadius.circular(5),
//                            child: Image.asset(
//                              'assets/images/song.jpg',
//                              height: 40,
//                            ),
//                          ),
//                          title: Text('One-line with leading widget'),
//                          subtitle: Text('Here is a second line'),
//                          trailing: Icon(SeteaIcons.play,
//                              color: Color(0xFFE9E9EA), size: 20),
//                        ),
//                      ),
//                      Container(
//                        margin: EdgeInsets.only(left: 20, right: 20),
//                        decoration: const BoxDecoration(
//                          border: Border(
//                            bottom:
//                                BorderSide(width: 2, color: Color(0xFFEAEAEA)),
//                          ),
//                        ),
//                        child: ListTile(
//                          leading: ClipRRect(
//                            borderRadius: BorderRadius.circular(5),
//                            child: Image.asset(
//                              'assets/images/song1.png',
//                              height: 40,
//                            ),
//                          ),
//                          title: Text('One-line with leading widget'),
//                          subtitle: Text('Here is a second line'),
//                          trailing: Icon(SeteaIcons.play,
//                              color: Color(0xFFE9E9EA), size: 20),
//                        ),
//                      ),
//                      Container(
//                        margin: EdgeInsets.only(left: 20, right: 20),
//                        decoration: const BoxDecoration(
//                          border: Border(
//                            bottom:
//                                BorderSide(width: 2, color: Color(0xFFEAEAEA)),
//                          ),
//                        ),
//                        child: ListTile(
//                          leading: ClipRRect(
//                            borderRadius: BorderRadius.circular(5),
//                            child: Image.asset(
//                              'assets/images/song.jpg',
//                              height: 40,
//                            ),
//                          ),
//                          title: Text('One-line with leading widget'),
//                          subtitle: Text('Here is a second line'),
//                          trailing: Icon(SeteaIcons.play,
//                              color: Color(0xFFE9E9EA), size: 20),
//                        ),
//                      ),
//                      Container(
//                        margin: EdgeInsets.only(left: 20, right: 20),
//                        decoration: const BoxDecoration(
//                          border: Border(
//                            bottom:
//                                BorderSide(width: 2, color: Color(0xFFEAEAEA)),
//                          ),
//                        ),
//                        child: ListTile(
//                          leading: ClipRRect(
//                            borderRadius: BorderRadius.circular(5),
//                            child: Image.asset(
//                              'assets/images/song.jpg',
//                              height: 40,
//                            ),
//                          ),
//                          title: Text('One-line with leading widget'),
//                          subtitle: Text('Here is a second line'),
//                          trailing: Icon(
//                            SeteaIcons.play,
//                            color: Color(0xFFE9E9EA),
//                            size: 20,
//                          ),
//                        ),
//                      ),
//                      Container(
//                        margin: EdgeInsets.only(left: 20, right: 20),
//                        decoration: const BoxDecoration(
//                          border: Border(
//                            bottom:
//                                BorderSide(width: 2, color: Color(0xFFEAEAEA)),
//                          ),
//                        ),
//                        child: ListTile(
//                          leading: ClipRRect(
//                            borderRadius: BorderRadius.circular(5),
//                            child: Image.asset(
//                              'assets/images/song.jpg',
//                              height: 40,
//                            ),
//                          ),
//                          title: Text('One-line with leading widget'),
//                          subtitle: Text('Here is a second line'),
//                          trailing: Icon(SeteaIcons.play,
//                              color: Color(0xFFE9E9EA), size: 20),
//                        ),
//                      ),
//                      SizedBox(
//                        height: 80,
//                      )
//                    ]),
//                  ),
//                ],
//              ),
//              Container(
//                child: Stack(
//                  children: <Widget>[
//                    Positioned(
//                      bottom: 0,
//                      left: 0,
//                      child: Container(
//                        padding:
//                            EdgeInsets.symmetric(vertical: 13, horizontal: 10),
//                        decoration: BoxDecoration(
//                          borderRadius: new BorderRadius.only(
//                            topLeft: const Radius.circular(30),
//                            topRight: const Radius.circular(30),
//                          ),
//                          color: Color(0xFFFffffff),
//                          boxShadow: [
//                            BoxShadow(
//                              color: Colors.black26,
//                              blurRadius: 30.0,
//                              // has the effect of softening the shadow
//                              spreadRadius: 0.3,
//                              // has the effect of extending the shadow
//                              offset: Offset(
//                                0.0, // horizontal, move right 10
//                                1.0, // vert
//                              ),
//                            )
//                          ],
//                        ),
//                        height: 75,
//                        width: MediaQuery.of(context).size.width,
//                        child: FlatButton(
//                          shape: new RoundedRectangleBorder(
//                            borderRadius: new BorderRadius.circular(22.0),
//                          ),
//                          color: Color(0xFF273522),
//                          textColor: Colors.white,
//                          onPressed: () {},
//                          child: Container(
//                              decoration: BoxDecoration(
//                                image: DecorationImage(
//                                  image:
//                                      AssetImage("assets/images/Intersect.png"),
//                                  fit: BoxFit.contain,
//                                ),
//                              ),
//                              width: 350,
//                              height: 100,
//                              child: Center(
//                                child: Text(
//                                  "Авторизироватся через Deezer",
//                                  style: TextStyle(
//                                    fontSize: 18,
//                                  ),
//                                  textAlign: TextAlign.center,
//                                ),
//                              )),
//                        ),
//                      ),
//                    )
//                  ],
//                ),
//              ),
//            ],
//          )),
//    );
//  }
//}

class MultimediaState extends State<Multimedia> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Material(
        color: Colors.white,
        child: CustomScrollView(
          slivers: [
            SliverPersistentHeader(
              delegate: MySliverAppBar(expandedHeight: 250),
              pinned: true,
            ),
            SliverList(
              delegate: SliverChildListDelegate([
                SizedBox(
                  height: 70,
                ),
                Container(
                  height: 320,
                  padding: EdgeInsets.all(12),
                  margin: EdgeInsets.only(left: 18,right: 18, bottom: 18),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10), color: Color(0xFFF8F8F8),
                    image: DecorationImage(
                      image: AssetImage("assets/images/articles.png"),
                      fit: BoxFit.cover,
                    ),
                  ),
                  child:
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text('Путешевствие', style: TextStyle(fontSize: 14, color: Colors.white, )),
                          Text('12.19.2020', style: TextStyle(fontSize: 14, color: Colors.white, ),)
                        ],
                      ),
                      Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            FlatButton(
                              padding: EdgeInsets.all(0),
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) => Articles()),
                                );
                              },
                              child:
                              Text('Название статьи максимум в несколько строк на конце троит...',
                                  style: TextStyle(fontSize: 18, color: Colors.white, fontWeight: FontWeight.w500)
                              ),
                            ),
                            SizedBox(height: 5,),
                            Text('Супер пупер стаья для таких как вы!',
                                style: TextStyle(fontSize: 14, color: Color(0xFF0AAAEB0), )
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ]),
            ),
          ],
        ),
      ),
    );
  }
}

class MySliverAppBar extends SliverPersistentHeaderDelegate {
  final double expandedHeight;

  MySliverAppBar({@required this.expandedHeight});

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Stack(
      fit: StackFit.expand,
      overflow: Overflow.visible,
      children: [
        AppBar(
          bottomOpacity: 0,
          elevation: 0,
          centerTitle: true,
          title: Image.asset(
            'assets/images/setea.png',
            width: 90,
            alignment: Alignment.center,
          ),
          flexibleSpace: Image(
            image: AssetImage('assets/images/profback.png'),
            fit: BoxFit.cover,
          ),
        ),
        Positioned(
          top: expandedHeight * 0.45 - shrinkOffset * 1.2,
          left: 10,
          child: Opacity(
            opacity: (1 - shrinkOffset / expandedHeight),
            child: SizedBox(
//                height: expandedHeight/2,
              width: MediaQuery.of(context).size.width * 0.95,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CookiesBox('Toxic Tea', 'Подходит вам сейчас',
                      'assets/images/stakan.png'),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  @override
  double get maxExtent => expandedHeight;

  @override
  double get minExtent => kToolbarHeight + 30;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) => true;
}

class WhereMusic extends StatelessWidget {
  String _executor;
  String _imageurl;

  WhereMusic({
    String imageurl = 'assets/images/yandex.png',
    String executor = 'Слушать в ЯндексМузыке',
  }) {
    _imageurl = imageurl;
    _executor = executor;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10), color: Color(0xFFF8F8F8)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(5),
              margin: EdgeInsets.only(right: 10),
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(10)),
              child: Image.asset(_imageurl),
            ),
            Flexible(
              child: Text(_executor,
                  style: TextStyle(
                      fontSize: 14,
                      color: Color(0xFF303030),
                      fontWeight: FontWeight.w500,
                      fontFamily: 'Rubik')),
            )
          ],
        ));
  }
}
