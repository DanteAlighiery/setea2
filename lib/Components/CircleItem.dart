import 'package:flutter/material.dart';
import '../Screens/Category.dart';
import 'package:google_fonts/google_fonts.dart';

var textStyle1 = TextStyle(fontSize: 22.0, color: Colors.grey[850], fontWeight:
FontWeight.w200, fontFamily: 'Rubik');

class CircleItem extends StatelessWidget {
   String _title;
   String _imageurl;


  CircleItem(this._title, this._imageurl, );

  @override
  Widget build(BuildContext context) {
  return  Container(
      margin: new EdgeInsets.symmetric(horizontal: 5.0),
    child: InkWell(onTap: () {
      Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => Category()),
    );},
      borderRadius:new BorderRadius.circular(15.0) ,
    child:Column(
          children: <Widget>[
            ClipRRect(
              borderRadius: new BorderRadius.circular(50.0),

              child: Container(
                color: Colors.grey[200],
                child:Image.asset(_imageurl,
                  height: 100, width: 100,  ),),),
            Text(_title, style:  GoogleFonts.rubik(textStyle: textStyle1), ),
          ] ),),);
  }



}
