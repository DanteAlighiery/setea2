import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import '../Screens/Category.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_fonts/google_fonts.dart';

var textStyle1 = TextStyle(fontSize: 24.0, color: Colors.grey[850], fontWeight:
FontWeight.bold, fontFamily: 'Rubik');
var textStyle2 = TextStyle(fontSize: 14.0, color: Color.fromRGBO(48,48,48,0.65), fontWeight:
FontWeight.w200, fontFamily: 'Rubik');
var textStyle3 = TextStyle(fontSize: 14.0, color: Color.fromRGBO(48,65,41,0.65), fontWeight:
FontWeight.w600, fontFamily: 'Rubik');

class Product extends StatelessWidget {
//  String _title;
//  String _imageurl;
//
//
//  Product(this._title, this._imageurl, );

  @override
  Widget build(BuildContext context) {
    return  Container(
      padding: EdgeInsets.all(12),
      child:Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
       Container(
      height:MediaQuery.of(context).size.height*0.2,
         width: MediaQuery.of(context).size.width*0.35,
         child:Stack(
            children: <Widget>[
              Positioned(
                left:3,
                top:12,
                child:Image.asset('assets/images/fon.png', height: MediaQuery.of(context).size.height*0.155,),),
      Positioned(
        left:6,
        child:Image.asset('assets/images/stakan.png', height: MediaQuery.of(context).size.height*0.190,),),
              Positioned(
                top:8,
                left:1,
                child:ClipRRect(
               borderRadius: BorderRadius.circular(50),
                child:Container(
                  padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
         color:Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.red,
                  blurRadius: 20.0, // has the effect of softening the shadow
                  spreadRadius: 5.0, // has the effect of extending the shadow
                  offset: Offset(
                    10.0, // horizontal, move right 10
                    10.0, // vertical, move down 10
                  ),
                )
              ],),
                  width:MediaQuery.of(context).size.height*0.060,
                  height:MediaQuery.of(context).size.height*0.060,
                  child:Image.asset('assets/images/lightning2.png', height: 1,),),
                ),
              )
            ],
          ),),
        Container(
          height:MediaQuery.of(context).size.height*0.21 ,
          width: MediaQuery.of(context).size.width*0.5,
          child:Column(
       mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(bottom: 6),
              child:Text('Toxic Tea',  style:GoogleFonts.rubik(textStyle: textStyle1))
            ),
            Container(
                margin: EdgeInsets.only(bottom: 6),
                child:Text('В несколько строчек состав чая и его ключевые особенные ингридиенты ингридиенты', style:GoogleFonts.rubik(textStyle: textStyle2),textAlign: TextAlign.start,)
            ),
            Container(
                margin: EdgeInsets.only(bottom: 6),
              padding: EdgeInsets.symmetric(horizontal:18, vertical:6),
                  decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  border: Border(
                    top: BorderSide(width: 1.0, color: Color(0xFF304129)),
                    left: BorderSide(width: 1.0, color: Color(0xFF304129)),
                    right: BorderSide(width: 1.0, color: Color(0xFF304129)),
                    bottom: BorderSide(width: 1.0, color: Color(0xFF304129)),
                  ),
                ),
                child:Text('200 ₽', style:GoogleFonts.rubik(textStyle: textStyle3))
              )

          ],
        ),),
        ],),
 );
  }



}
