import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';


var textStyle = TextStyle(fontSize: 32.0, color: Colors.black, fontWeight:
FontWeight.w600,fontFamily: 'Rubik' );
var textStyle1 = TextStyle(fontSize: 24.0, color: Colors.grey[850], fontWeight:
FontWeight.w600, fontFamily: 'Rubik');
var textStyle2 = TextStyle(fontSize: 20.0, color: Colors.black54, fontWeight:
FontWeight.w100, fontFamily: 'Rubik');
var textStyle3 = TextStyle(fontSize: 18.0, color: Colors.green, fontWeight:
FontWeight.w100, fontFamily: 'Rubik');
var textStyle5 = TextStyle(fontSize: 22.0, color: Colors.black, fontWeight:
FontWeight.w500, fontFamily: 'Rubik');
var textStyle4 = TextStyle(fontSize: 26.0, color: Colors.green, fontWeight:
FontWeight.w900, fontFamily: 'Rubik');
var textStyle6 = TextStyle(fontSize: 20.0, color: Colors.grey[400], fontWeight:
FontWeight.normal, fontFamily: 'Rubik');
var textStyle7 = TextStyle(fontSize: 20.0, color: Colors.green, fontWeight:
FontWeight.w200, fontFamily: 'Rubik');
var textStyle8 = TextStyle(fontSize: 20.0, color: Colors.black, fontWeight:
FontWeight.w200, fontFamily: 'Rubik');


class Energy extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return    Container(
      margin: new EdgeInsets.symmetric(horizontal:12.0,),
      padding: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
      decoration: BoxDecoration(
          color: Color(0xFF273522),
          borderRadius: BorderRadius.all(Radius.circular(15)),
          image: DecorationImage(
            image: AssetImage("assets/images/Intersect.png"),
            fit: BoxFit.fitHeight,
          ),
      ),
      child: Column(
        children: <Widget>[
            Container(
              padding: EdgeInsets.all(15),
              decoration: BoxDecoration(
                color: Color(0xFFffffff),
                borderRadius: BorderRadius.all(Radius.circular(10))
              ),
              child: Image.asset('assets/images/lightning.png'),
            ),
          ConstrainedBox(
            constraints: BoxConstraints(maxHeight: 150,),
            child: RotatedBox(
              quarterTurns: 3,
              child: RichText(
                text: TextSpan(
                    text: 'Энергичное настроение',
                    style: TextStyle(fontSize: 18, color: Colors.white, fontWeight:
                    FontWeight.w400,fontFamily: 'Rubik' )
                ),
              ),
            ),
          ),


        ],
      )
    );
  }



}
