import 'package:camera/camera.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:setea2/setea_icons_icons.dart';
var textStyle = TextStyle(
    fontSize: 32.0,
    color: Colors.black,
    fontWeight: FontWeight.w600,
    fontFamily: 'Rubik');
var textStyle1 = TextStyle(
    fontSize: 24.0,
    color: Colors.grey[850],
    fontWeight: FontWeight.w200,
    fontFamily: 'Rubik');
var textStyle2 = TextStyle(
    fontSize: 20.0,
    color: Colors.black54,
    fontWeight: FontWeight.w100,
    fontFamily: 'Rubik');
var textStyle3 = TextStyle(
    fontSize: 18.0,
    color: Colors.green,
    fontWeight: FontWeight.w100,
    fontFamily: 'Rubik');
var textStyle5 = TextStyle(
    fontSize: 22.0,
    color: Colors.black,
    fontWeight: FontWeight.w500,
    fontFamily: 'Rubik');
var textStyle4 = TextStyle(
    fontSize: 26.0,
    color: Colors.green,
    fontWeight: FontWeight.w900,
    fontFamily: 'Rubik');
var textStyle6 = TextStyle(
    fontSize: 20.0,
    color: Colors.grey[400],
    fontWeight: FontWeight.normal,
    fontFamily: 'Rubik');
var textStyle7 = TextStyle(
    fontSize: 20.0,
    color: Colors.green,
    fontWeight: FontWeight.w200,
    fontFamily: 'Rubik');
var textStyle8 = TextStyle(
    fontSize: 20.0,
    color: Colors.black,
    fontWeight: FontWeight.w200,
    fontFamily: 'Rubik');

class TransitionAppBar extends StatelessWidget {
  final Widget avatar;
  final Widget title;
  final double extent;

  TransitionAppBar({this.avatar, this.title, this.extent = 120, Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverPersistentHeader(
      pinned: true,
      floating: false,
      delegate: _TransitionAppBarDelegate(
          avatar: avatar,
          title: title,
          extent: extent > 100 ? extent : 100
      ),
    );
  }
}

class _TransitionAppBarDelegate extends SliverPersistentHeaderDelegate {
  final _avatarMarginTween = EdgeInsetsTween(
      begin: EdgeInsets.only(bottom: 20, left: 10),
      end: EdgeInsets.only(left: 25.0, top: 25.0));
  final _avatarAlignTween =
  AlignmentTween(begin: Alignment.bottomLeft, end: Alignment.center);

  final Widget avatar;
  final Widget title;
  final double extent;

  _TransitionAppBarDelegate({this.avatar, this.title, this.extent = 50})
      : assert(avatar != null),
        assert(extent == null || extent >= 100),
        assert(title != null);

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    double tempVal = 34 * maxExtent / 100;
    final progress =  shrinkOffset > tempVal ? 0.9 : shrinkOffset / tempVal;
    print("Objechjkf === ${progress} ${shrinkOffset}");
    final avatarMargin = _avatarMarginTween.lerp(progress);
    final avatarAlign = _avatarAlignTween.lerp(progress);

    return Container(
      child:Stack(
      children: <Widget>[
        Container(
          color: Colors.white70,
        ),
        AnimatedContainer(
          duration: Duration(milliseconds: 100),
          height: 150,
          constraints: BoxConstraints(maxHeight: 85),
        ),

        Container(
          margin: EdgeInsets.only(left: 10,top:25),
          child:IconButton(
            icon: const Icon(SimpleLineIcons.menu, color: Colors.green, size: 30),
            onPressed: () {
              Scaffold.of(context).openDrawer();
            },
          ),),
        Container(
          padding: avatarMargin,
          child: Align(
              alignment: avatarAlign,
              child: Text('Магазин', style: textStyle,textScaleFactor: 1.1-progress/2.5)
          ),
        ),

      ],
      ), );
  }

  @override
  double get maxExtent => extent;

  @override
  double get minExtent => (maxExtent * 50) / 100;

  @override
  bool shouldRebuild(_TransitionAppBarDelegate oldDelegate) {
    return avatar != oldDelegate.avatar || title != oldDelegate.title;
  }
}