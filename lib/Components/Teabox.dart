import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:setea2/Screens/CardTovar.dart';
import 'package:google_fonts/google_fonts.dart';
var textStyle = TextStyle(fontSize: 32.0, color: Colors.black, fontWeight:
FontWeight.w600,fontFamily: 'Rubik' );
var textStyle1 = TextStyle(fontSize: 24.0, color: Colors.grey[850], fontWeight:
FontWeight.w200, fontFamily: 'Rubik');
var textStyle2 = TextStyle(fontSize: 20.0, color: Colors.black54, fontWeight:
FontWeight.w100, fontFamily: 'Rubik');
var textStyle3 = TextStyle(fontSize: 18.0, color: Colors.green, fontWeight:
FontWeight.w100, fontFamily: 'Rubik');
var textStyle5 = TextStyle(fontSize: 22.0, color: Colors.black, fontWeight:
FontWeight.w500, fontFamily: 'Rubik');
var textStyle4 = TextStyle(fontSize: 26.0, color: Colors.green, fontWeight:
FontWeight.w900, fontFamily: 'Rubik');
var textStyle6 = TextStyle(fontSize: 20.0, color: Colors.grey[400], fontWeight:
FontWeight.normal, fontFamily: 'Rubik');
var textStyle7 = TextStyle(fontSize: 20.0, color: Colors.green, fontWeight:
FontWeight.w200, fontFamily: 'Rubik');
var textStyle8 = TextStyle(fontSize: 20.0, color: Colors.black, fontWeight:
FontWeight.w200, fontFamily: 'Rubik');
class Teabox extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return    Container(
      margin: new EdgeInsets.symmetric(horizontal:12.0),
      decoration: new BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.grey[400],
            blurRadius: 15.0, // has the effect of softening the shadow
            spreadRadius: 0.5,
            // has the effect of extending the shadow
            offset: Offset(
              2.0, // horizontal, move right 10
              6.0, // vertical, move down 10
            ),
          )
        ],),
      child:ClipRRect(
        borderRadius: new BorderRadius.circular(20),
        child:Container(
          padding: new EdgeInsets.only(top: 6.0, left:6.0, right:6.0, bottom: 6.0),

          color: Colors.grey[200],
          child:Row(
             mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only( top:12.0, left:12.0, ),
                      child:Text('Toxic Tea', style:GoogleFonts.rubik(textStyle: textStyle,),  ),),
                    Padding(
                      padding: EdgeInsets.only( top:6.0, left:12.0, bottom: 20.0),
                      child:Text('Подходит вам сейчас', style:GoogleFonts.rubik(textStyle: textStyle2,), ),),
                    InkWell(onTap: () {  Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => CardTovar()),
                    );},
                    child:Row(children: <Widget>[
                      Container(
                        padding: EdgeInsets.only( top:6.0, left:12.0, bottom: 12.0),
                        child:Text('Подробнее', style:GoogleFonts.rubik(textStyle: textStyle3,), ),),
                      Container(
                        padding: EdgeInsets.only( top:6.0, left:3.0, bottom: 12.0),
                        child:Icon(Ionicons.ios_arrow_round_forward, color: Colors.green, size: 35,),)
                    ],),),
                  ]
              ),
              Container(
                margin: new EdgeInsets.only(right: 6),
                alignment: Alignment.centerRight,
                  child:Image.asset('assets/images/cuppp.png',
                    height: 100, width: 100, alignment: Alignment.centerRight, ),)
            ],

          ),
        ),),);
  }



}
