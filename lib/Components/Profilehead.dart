import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:google_fonts/google_fonts.dart';
var textStyle = TextStyle(fontSize: 32.0, color: Colors.white, fontWeight:
FontWeight.w600,fontFamily: 'Rubik' );
var textStyle1 = TextStyle(fontSize: 24.0, color: Colors.grey[850], fontWeight:
FontWeight.w200, fontFamily: 'Rubik');
var textStyle2 = TextStyle(fontSize: 20.0, color: Colors.black54, fontWeight:
FontWeight.w100, fontFamily: 'Rubik');
var textStyle3 = TextStyle(fontSize: 18.0, color: Colors.green, fontWeight:
FontWeight.w100, fontFamily: 'Rubik');
var textStyle5 = TextStyle(fontSize: 20.0, color: Colors.white60, fontWeight:
FontWeight.w300, fontFamily: 'Rubik');
var textStyle4 = TextStyle(fontSize: 26.0, color: Colors.green, fontWeight:
FontWeight.w900, fontFamily: 'Rubik');
var textStyle6 = TextStyle(fontSize: 20.0, color: Colors.grey[400], fontWeight:
FontWeight.normal, fontFamily: 'Rubik');
var textStyle7 = TextStyle(fontSize: 20.0, color: Colors.green, fontWeight:
FontWeight.w200, fontFamily: 'Rubik');
var textStyle8 = TextStyle(fontSize: 20.0, color: Colors.black, fontWeight:
FontWeight.w200, fontFamily: 'Rubik');
class Profilehead extends StatelessWidget {

  String _name;
  String _imageurl;
  Profilehead(this._imageurl, this._name);
  @override
  Widget build(BuildContext context) {
    return Container(

      margin: new EdgeInsets.only(top:10, left:12, bottom: 18),
        child:Row(
            children: <Widget>[
              ClipRRect(
                borderRadius: new BorderRadius.circular(10.0),

                    child:Image.network(_imageurl,height: 100,width: 100,fit: BoxFit.cover, )
                ),
               Column(
                 crossAxisAlignment:  CrossAxisAlignment.start,
                 mainAxisAlignment: MainAxisAlignment.center,

                 children: <Widget>[
                   Container(
                       margin: new EdgeInsets.only( left:12,bottom:3 ),
                     child:Text('С возвращением!', style:  GoogleFonts.rubik(textStyle: textStyle5),)
                   ),
                   Container(
                       margin: new EdgeInsets.only( left:12,top:3 ),
                     child:Text(_name, style:  GoogleFonts.rubik(textStyle: textStyle),)
                   ),

                 ],
               )
            ]),


    );
  }



}
