import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';

class LinedItem extends StatefulWidget {
  @override
  createState() => new LinedItemState();
}

class LinedItemState extends State<LinedItem> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 18),
      decoration: BoxDecoration(
          color: Color(0xFFF8F8F8), borderRadius: BorderRadius.circular(15)),
      padding: EdgeInsets.only(top: 12, bottom: 6, right: 12, left: 12),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(right: 12),
                width: 55,
                height: 55,
                decoration: BoxDecoration(
                  color: Color(0xFF273522),
                  shape: BoxShape.circle,
                ),
                child: Stack(
//                  fit: StackFit.expand,
                  overflow: Overflow.visible,
                  children: <Widget>[
                    Positioned(
                        left: 8,
                        top: -10,
                        child: Image.asset(
                          'assets/images/cup5.png',
                          width: 40,
                        )),

                  ],
                ),
              ),
              Column(
                children: <Widget>[
                  Text(
                    'Toxic tea',
                    style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.w200,
                        color: Color(0xFF303030)),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.circle,
                        ),
                        padding: EdgeInsets.all(6),
                        margin: EdgeInsets.only(right: 12),
                        child: Text(
                          '-',
                          style:
                          TextStyle(color: Color(0xFF747474), fontSize: 24),
                        ),
                      ),
                      Text(
                        '1',
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w200,
                            color: Color(0xFF273522)),
                      ),
                      Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.circle,
                        ),
                        padding: EdgeInsets.all(6),
                        margin: EdgeInsets.only(left: 12),
                        child: Text(
                          '+',
                          style:
                          TextStyle(color: Color(0xFF747474), fontSize: 24),
                        ),
                      ),
                    ],
                  )
                ],
              )
            ],
          ),
          Column(
            children: <Widget>[
              Text(
                '\$5',
                style: TextStyle(
                    color: Color(0xFF273522),
                    fontSize: 24,
                    fontWeight: FontWeight.w700),
              ),
              SizedBox(
                height: 20,
              ),
      Icon(Ionicons.ios_arrow_round_forward, color: Colors.grey, size: 35,),
            ],
          )
        ],
      ),
    );
  }
}
