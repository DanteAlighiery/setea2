import 'package:flutter/material.dart';
var textStyle1 = TextStyle(fontSize: 24.0, color: Colors.grey[850], fontWeight:
FontWeight.w200, fontFamily: 'Rubik');

class CustomAppBar extends StatelessWidget {
  String _title;
  String _imageurl;

  CustomAppBar(this._title, this._imageurl);

  @override
  Widget build(BuildContext context) {
    return  Container(
      margin: new EdgeInsets.symmetric(horizontal: 10.0),
      child: InkWell(onTap: () {},
        child:Column(
            children: <Widget>[
              ClipRRect(
                borderRadius: new BorderRadius.circular(50.0),

                child: Container(
                  color: Colors.grey[200],
                  child:Image.asset(_imageurl,
                    height: 100, width: 100,  ),),),
              Text(_title, style: textStyle1, ),
            ] ),),);
  }



}
