import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import '../Screens/CardTovar.dart';

var textStyle = TextStyle(
    fontSize: 32.0,
    color: Colors.black,
    fontWeight: FontWeight.w600,
    fontFamily: 'Rubik');
var textStyle1 = TextStyle(
    fontSize: 24.0,
    color: Colors.grey[850],
    fontWeight: FontWeight.w200,
    fontFamily: 'Rubik');
var textStyle2 = TextStyle(
    fontSize: 20.0,
    color: Colors.black54,
    fontWeight: FontWeight.w100,
    fontFamily: 'Rubik');
var textStyle3 = TextStyle(
    fontSize: 18.0,
    color: Colors.green,
    fontWeight: FontWeight.w100,
    fontFamily: 'Rubik');
var textStyle5 = TextStyle(
    fontSize: 22.0,
    color: Colors.black,
    fontWeight: FontWeight.w500,
    fontFamily: 'Rubik');
var textStyle4 = TextStyle(
    fontSize: 26.0,
    color: Colors.green,
    fontWeight: FontWeight.w900,
    fontFamily: 'Rubik');
var textStyle6 = TextStyle(
    fontSize: 20.0,
    color: Colors.grey[400],
    fontWeight: FontWeight.normal,
    fontFamily: 'Rubik');
var textStyle7 = TextStyle(
    fontSize: 20.0,
    color: Colors.green,
    fontWeight: FontWeight.w200,
    fontFamily: 'Rubik');
var textStyle8 = TextStyle(
    fontSize: 20.0,
    color: Colors.black,
    fontWeight: FontWeight.w200,
    fontFamily: 'Rubik');

class Biggerteacup extends StatelessWidget {
  String _title;
  String _price;
  String _imageurl;

  Biggerteacup(this._title, this._price, this._imageurl);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: new EdgeInsets.symmetric(horizontal: 12.0, vertical: 8),
        child: InkWell(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => CardTovar()),
              );
            },
            borderRadius: new BorderRadius.circular(15.0),
            child:
            Column(
              children: <Widget>[
                ClipRRect(
                  borderRadius: new BorderRadius.only(
                      topLeft: const Radius.circular(15.0),
                      topRight: const Radius.circular(15.0),
                      bottomLeft: const Radius.circular(15.0),
                      bottomRight: const Radius.circular(15.0)),
                  child: Container(
                    width: 160,
                    height: 230,
                    color: Colors.grey[200],
                    padding: EdgeInsets.symmetric(horizontal: 14, vertical:5),


                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[

                        Align(
                          child: Text(
                            _title,
                            style: textStyle1,
                          ),
                          alignment: Alignment.centerLeft,
                        ),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                '\$' + _price,
                                style: textStyle4,
                              ),
                              Icon(
                                Ionicons.ios_arrow_round_forward,
                                color: Colors.grey[400],
                                size: 45,
                              ),
                            ]),
                      ],
                    ),
                  ),
                ),
                Container(
                  height: 10,
                  width: 120,
                  child:
                  Stack(
                    fit: StackFit.expand,
                    overflow: Overflow.visible,
                    children: <Widget>[
                      Positioned(
                        top: -250,

                        child:
                        Image.asset(
                          _imageurl,
                          height: 160,
                          width: 120,
                        ),
                      )
                    ],
                  ),
                ),
              ],
            )

        ));
  }
}
