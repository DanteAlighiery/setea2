import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';


class Articles extends StatelessWidget {

  String _name;
  String _imageurl;

  Articles(
      {String imageurl = 'assets/images/articles.png', String name = 'Название статьи в две строки в конце троиточие'}) {
    _imageurl = imageurl;
    _name = name;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        width: 311,
        margin: EdgeInsets.symmetric(horizontal: 10),
        decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(_imageurl),
              fit: BoxFit.cover,
            ),
            borderRadius: new BorderRadius.circular(15),
        ),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: new BorderRadius.circular(15),
            color: Colors.white,
          ),
          margin: EdgeInsets.only(bottom:15, left: 15, right: 15),
//          padding: EdgeInsets.all(10),
          padding: EdgeInsets.only(top: 10, left: 10, bottom: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Flexible(
                child: Text(
                  '$_name...',
                  style: TextStyle(fontSize: 16, fontFamily: 'Rubik', color: Color(0xFF5A5A5A)),
                ),
              ),
              Icon(Ionicons.md_more, size:40, color: Color(0xFF0EAEAEA),)
            ],
          ),
        ),
        alignment: Alignment.bottomCenter,
    );
  }
}

