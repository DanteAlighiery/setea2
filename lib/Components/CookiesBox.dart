import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:setea2/Screens/CardTovar.dart';
import 'package:google_fonts/google_fonts.dart';

var textStyle = TextStyle(
    fontSize: 32.0,
    color: Colors.black,
    fontWeight: FontWeight.w600,
    fontFamily: 'Rubik');
var textStyle1 = TextStyle(
    fontSize: 24.0,
    color: Colors.grey[850],
    fontWeight: FontWeight.w200,
    fontFamily: 'Rubik');
var textStyle2 = TextStyle(
    fontSize: 18.0,
    color: Colors.black54,
    fontWeight: FontWeight.w100,
    fontFamily: 'Rubik');
var textStyle3 = TextStyle(
    fontSize: 18.0,
    color: Color(0xFF304129),
    fontWeight: FontWeight.w100,
    fontFamily: 'Rubik');
var textStyle5 = TextStyle(
    fontSize: 22.0,
    color: Colors.black,
    fontWeight: FontWeight.w500,
    fontFamily: 'Rubik');
var textStyle4 = TextStyle(
    fontSize: 26.0,
    color: Colors.green,
    fontWeight: FontWeight.w900,
    fontFamily: 'Rubik');
var textStyle6 = TextStyle(
    fontSize: 20.0,
    color: Colors.grey[400],
    fontWeight: FontWeight.normal,
    fontFamily: 'Rubik');
var textStyle7 = TextStyle(
    fontSize: 20.0,
    color: Colors.green,
    fontWeight: FontWeight.w200,
    fontFamily: 'Rubik');
var textStyle8 = TextStyle(
    fontSize: 20.0,
    color: Colors.black,
    fontWeight: FontWeight.w200,
    fontFamily: 'Rubik');

class CookiesBox extends StatelessWidget {
  String _title;
  String _subtitle;
  String _imageurl;

  CookiesBox(
    this._title,
    this._subtitle,
    this._imageurl,
  );

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: new EdgeInsets.symmetric(horizontal: 12.0),
      decoration: new BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        boxShadow: [
          BoxShadow(
            color: Colors.black45,
            blurRadius:80.0, // has the effect of softening the shadow
            spreadRadius: 0.3,
            // has the effect of extending the shadow
            offset: Offset(
              0.0, // horizontal, move right 10
              4.0, // vert
            ),
          )
        ],
      ),
      child: ClipRRect(
        borderRadius: new BorderRadius.circular(20),
        child: Container(
          padding: new EdgeInsets.all(6),
          color: Colors.grey[200],
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: 12.0,
                        vertical: 5
                      ),
                      child: Text(
                        _title,
                        style: GoogleFonts.rubik(textStyle: textStyle),
                      ),
                    ),
                    Container(
                      padding:
                          EdgeInsets.only(top: 6.0, left: 12.0, bottom: 10.0),
                      child: Text(
                        _subtitle,
                        style: GoogleFonts.rubik(textStyle: TextStyle(
                            fontSize: 18.0,
                            color: Colors.black54,
                            fontWeight: FontWeight.w100,
                            fontFamily: 'Rubik')
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => CardTovar()),
                        );
                      },
                      child: Row(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(
                                top: 6.0, left: 12.0, bottom: 12.0),
                            child: Text(
                              'Подробнее',
                              style: GoogleFonts.rubik(
                                  textStyle: TextStyle(
                                      fontSize: 18.0,
                                      color: Color(0xFF304129),
                                      fontWeight: FontWeight.w100,
                                      fontFamily: 'Rubik')),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(
                                top: 6.0, left: 3.0, bottom: 12.0),
                            child: Icon(
                              Ionicons.ios_arrow_round_forward,
                              color: Color(0xFF304129),
                              size: 35,
                            ),
                          )
                        ],
                      ),
                    ),
                  ]),
              Container(
                width: 95,
                height: 95,
                decoration: BoxDecoration(
                  color: Color(0xFF273522),
                  shape: BoxShape.circle,
                ),
                child: Stack(
//                  fit: StackFit.expand,
                  overflow: Overflow.visible,
                  children: <Widget>[
                    Positioned(
                        left: -10,
                        top: -20,
                        child: Image.asset(
                          _imageurl,
                          width: 100,
                          height: 120,
                        )),
                    Positioned(
                      top:-10,
                      left:-10,
                      child:ClipRRect(
                        borderRadius: BorderRadius.circular(50),
                        child:Container(
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                            color:Colors.white,
                            boxShadow: [
                              BoxShadow(
                                color: Colors.red,
                                blurRadius: 20.0, // has the effect of softening the shadow
                                spreadRadius: 5.0, // has the effect of extending the shadow
                                offset: Offset(
                                  10.0, // horizontal, move right 10
                                  10.0, // vertical, move down 10
                                ),
                              )
                            ],),
                          width:MediaQuery.of(context).size.height*0.060,
                          height:MediaQuery.of(context).size.height*0.060,
                          child:Image.asset('assets/images/lightning2.png', height: 1,),),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
