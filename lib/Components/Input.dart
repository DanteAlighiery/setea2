import 'package:flutter/material.dart';
import 'package:setea2/setea_icons_icons.dart';
var textStyle1 = TextStyle(fontSize: 16.0, color: Colors.grey[850], fontWeight:
FontWeight.w200, fontFamily: 'Rubik');

class Input extends StatelessWidget {

  final _controller;
  String _text;

  Input(this._controller);

  @override
  Widget build(BuildContext context) {
    return  Container(
     child: Container(
       decoration: const BoxDecoration(
         border: Border(
           bottom: BorderSide(width: 1.0, color: Color(0xFFEAEAEA)),
         ),
       ),
       child:Row(
         mainAxisAlignment: MainAxisAlignment.spaceBetween,
         children: <Widget>[
               TextField(controller:_controller),
         Icon(SeteaIcons.pencil, color: Colors.black)
         ],
       ),
     ),

    );
  }



}
