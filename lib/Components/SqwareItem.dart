import 'package:flutter/material.dart';
var textStyle1 = TextStyle(fontSize: 24.0, color: Colors.grey[850], fontWeight:
FontWeight.w200, fontFamily: 'Rubik');

class SqwareItem extends StatelessWidget {

  String _imageurl;

  SqwareItem( this._imageurl);

  @override
  Widget build(BuildContext context) {
    return  Container(
      margin: new EdgeInsets.symmetric(horizontal: 8.0, vertical: 12.0),
      child: InkWell(onTap: () {},
        child:Column(
            children: <Widget>[
              ClipRRect(
                borderRadius: new BorderRadius.circular(20.0),
                child: Container(
                  color: Colors.grey[300],
                  child:Image.asset(_imageurl,
                    height: MediaQuery.of(context).size.width*0.185, width: MediaQuery.of(context).size.width*0.185,  ),),),

            ] ),),);
  }



}
