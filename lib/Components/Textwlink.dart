import 'package:flutter/material.dart';
import 'package:setea2/setea_icons_icons.dart';
import 'package:google_fonts/google_fonts.dart';
var textStyle1 = TextStyle(fontSize: 18.0, color: Colors.grey[850], fontWeight:
FontWeight.w200, fontFamily: 'Rubik');

class Textwlink extends StatelessWidget {
  String _title;


  Textwlink(this._title);

  @override
  Widget build(BuildContext context) {
    return  Container(
      margin: new EdgeInsets.symmetric(horizontal: 18.0,),

      decoration: const BoxDecoration(
        border: Border(
          bottom: BorderSide(width: 1.0, color: Color(0xFFEAEAEA)),
        ),
      ),
      child: InkWell(onTap: () {},

        child:Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                margin: new EdgeInsets.symmetric(vertical: 20.0),
              child:Text(_title,  style:  GoogleFonts.rubik(textStyle: textStyle1), ),),
              Container(
                margin: new EdgeInsets.only(right: 18.0),
              child:Icon(SeteaIcons.arrow, size:13, color:Color(0xFFEAEAEA)),),
            ] ),),);
  }



}
