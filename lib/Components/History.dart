import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'SqwareItem.dart';
import 'package:google_fonts/google_fonts.dart';
var textStyle = TextStyle(fontSize: 26.0, color: Colors.black, fontWeight:
FontWeight.w600,fontFamily: 'Rubik' );
var textStyle1 = TextStyle(fontSize: 20.0, color: Colors.grey[850], fontWeight:
FontWeight.w600, fontFamily: 'Rubik');
var textStyle2 = TextStyle(fontSize: 20.0, color: Colors.black54, fontWeight:
FontWeight.w100, fontFamily: 'Rubik');
var textStyle3 = TextStyle(fontSize: 14.0, color: Colors.green, fontWeight:
FontWeight.w100, fontFamily: 'Rubik');
var textStyle5 = TextStyle(fontSize: 22.0, color: Colors.black, fontWeight:
FontWeight.w500, fontFamily: 'Rubik');
var textStyle4 = TextStyle(fontSize: 26.0, color: Colors.green, fontWeight:
FontWeight.w900, fontFamily: 'Rubik');
var textStyle6 = TextStyle(fontSize: 20.0, color: Colors.grey[400], fontWeight:
FontWeight.normal, fontFamily: 'Rubik');
var textStyle7 = TextStyle(fontSize: 20.0, color: Colors.green, fontWeight:
FontWeight.w200, fontFamily: 'Rubik');
var textStyle8 = TextStyle(fontSize: 20.0, color: Colors.black, fontWeight:
FontWeight.w200, fontFamily: 'Rubik');
class History extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return    Container(
      margin: new EdgeInsets.symmetric(horizontal:12.0, vertical: 24),
      decoration: new BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.grey[400],
            blurRadius: 15.0, // has the effect of softening the shadow
            spreadRadius: 0.5,
            // has the effect of extending the shadow
            offset: Offset(
              2.0, // horizontal, move right 10
              6.0, // vertical, move down 10
            ),
          )
        ],),
      child:ClipRRect(
        borderRadius: new BorderRadius.circular(20),
        child:Container(
          padding: new EdgeInsets.only(top: 6.0, left:6.0, right:6.0, bottom: 6.0),
          color: Colors.grey[200],
          child:Wrap(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    padding: new EdgeInsets.only(top:6, left:12),
                  child:Text('История Покупок',  style:  GoogleFonts.rubik(textStyle: textStyle1),),),
                  Row(
                    children: <Widget>[
                      Container(
                        alignment: Alignment.centerLeft,
                        padding: new EdgeInsets.only(top:12, ),
                        child:Text('Подробнее', style:  GoogleFonts.rubik(textStyle: textStyle3), ),),
                      Container(
                        alignment: Alignment.centerRight,
                        padding: new EdgeInsets.only(top:12, right:1  ),
                        child:Icon(Ionicons.ios_arrow_round_forward, color: Colors.green, size: 35,),)
                    ],
                  ),
                ],
              ),
              Row(
                  children: <Widget>[
                    SqwareItem('assets/images/cup3.png'),
                    SqwareItem('assets/images/cup2.png'),
                    SqwareItem('assets/images/cup4.png'),
                    SqwareItem('assets/images/cup5.png'),
                    ]
              )
            ],
          )
        ),),);
  }



}
