import 'package:setea2/setea_icons_icons.dart';
import 'package:setea2/setea2_icons.dart';
import 'package:google_fonts/google_fonts.dart';

import './Screens/Camera.dart';
import 'package:camera/camera.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';

import 'Screens/Shop.dart';
import 'Screens/Scan.dart';
import 'Screens/Profile.dart';
import 'Screens/Hello.dart';
import 'Screens/Multimedia.dart';
import 'Components/BottomNavyBar.dart';
import 'dart:async';
import 'dart:io';
import 'Screens/Cameraras.dart';

Future<Null> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  cameras = await availableCameras();
  runApp(new MyApp());
}


final String icon1 = 'assets/images/icon1.svg';

var textStyle = TextStyle(fontSize: 24.0, color: Colors.white, fontWeight:
FontWeight.w600,fontFamily: 'Rubik' );
var textStyle1 = TextStyle(fontSize: 24.0, color: Colors.grey[850], fontWeight:
FontWeight.w200, fontFamily: 'Rubik');
var textStyle2 = TextStyle(fontSize: 20.0, color: Colors.black54, fontWeight:
FontWeight.w100, fontFamily: 'Rubik');
var textStyle3 = TextStyle(fontSize: 18.0, color: Colors.green, fontWeight:
FontWeight.w100, fontFamily: 'Rubik');
var textStyle5 = TextStyle(fontSize: 16.0, color: Colors.white60, fontWeight:
FontWeight.w300, fontFamily: 'Rubik');
var textStyle4 = TextStyle(fontSize: 26.0, color: Colors.green, fontWeight:
FontWeight.w900, fontFamily: 'Rubik');
var textStyle6 = TextStyle(fontSize: 14.0, color: Colors.white60, fontWeight:
FontWeight.w300, fontFamily: 'Rubik');
var textStyle7 = TextStyle(fontSize: 20.0, color: Colors.green, fontWeight:
FontWeight.w200, fontFamily: 'Rubik');
var textStyle8 = TextStyle(fontSize: 20.0, color: Colors.black, fontWeight:
FontWeight.w200, fontFamily: 'Rubik');

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false, // скрываем надпись debug
      theme: ThemeData(
        fontFamily: 'Rubik',
        primaryColor: Color(0xFFFFFFFF),
        primaryColorDark: Color(0xFFFFFFFF),
      ),
      home: HomePage(),

    );
  }
}





class HomePage extends StatefulWidget {

  @override
  createState() => new HomePageState();
}

class HomePageState extends State<HomePage> {
  int _currentIndex = 0;
  PageController _pageController;

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer:
      Theme(
        data: Theme.of(context).copyWith(
          canvasColor: Colors.black87, //This will change the drawer background to blue.
          //other styles
        ),
        child:Drawer(
          child: ListView(
            // Important: Remove any padding from the ListView.
            padding: EdgeInsets.zero,
            children: <Widget>[
              DrawerHeader(
                child:ListTile(
                  leading:ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child:Image.network('https://images.unsplash.com/photo-1507003211169-0a1dd7228f2d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80',
                      fit:BoxFit.cover, width: MediaQuery.of(context).size.width*0.13, height: MediaQuery.of(context).size.width*0.13 , ),),
                  title: Text('С возвращением!',style:GoogleFonts.rubik(textStyle: textStyle5,),),
                  subtitle: Text('CurrentUser',  style:GoogleFonts.rubik(textStyle: textStyle,)),

                  onTap: () { },
                ),

              ),
              Column(
                children: <Widget>[
                  ListTile(
                    contentPadding:EdgeInsets.only(left:25),
                    leading: Icon(SeteaIcons.star, color: Colors.white60),
                    title:Align(child:Text('Рекомендация',style: textStyle5, textAlign: TextAlign.start,),
                      alignment: Alignment(-1.2, 0),
                    ),
                    onTap: () { },
                  ),     ListTile(
                    contentPadding:EdgeInsets.only(left:25),
                    leading: Icon(SeteaIcons.time, color: Colors.white60),
                    title: Align(child:Text('История покупок',style:GoogleFonts.rubik(textStyle: textStyle5), textAlign: TextAlign.start,),
                      alignment: Alignment(-1.25, 0),
                    ),
                    onTap: () { },
                  ),     ListTile(
                    contentPadding:EdgeInsets.only(left:25),
                    leading: Icon(SeteaIcons.question, color: Colors.white60),
                    title: Align(child:Text('Часты вопросы ',style:GoogleFonts.rubik(textStyle: textStyle5), textAlign: TextAlign.start,),
                      alignment: Alignment(-1.25, 0),
                    ),
                    onTap: () { },
                  ),     ListTile(
                    contentPadding:EdgeInsets.only(left:25),
                    leading: Icon(SeteaIcons.headphones, color: Colors.white60),
                    title: Align(child:Text('Поддержка',style:GoogleFonts.rubik(textStyle: textStyle5), textAlign: TextAlign.start,),
                      alignment: Alignment(-1.2, 0),
                    ),
                    onTap: () { },
                  ),     ListTile(
                    contentPadding:EdgeInsets.only(left:25),
                    leading: Icon(SeteaIcons.info, color: Colors.white60),
                    title: Align(child:Text('О компании',style:GoogleFonts.rubik(textStyle: textStyle5), textAlign: TextAlign.start,),
                      alignment: Alignment(-1.2, 0),
                    ),
                    onTap: () { },
                  ),     ListTile(
                    contentPadding:EdgeInsets.only(left:25),
                    leading: Icon(SeteaIcons.friend, color: Colors.white60),
                    title: Align(child:Text('Пригласить друга',style:GoogleFonts.rubik(textStyle: textStyle5), textAlign: TextAlign.start,),
                      alignment: Alignment(-1.3, 0),
                    ),
                    onTap: () { },
                  ),
                ],
              ),
              Container(
                  margin: EdgeInsets.only(top:240, left: 26),
                  child:Row(
                    children: <Widget>[
                      InkWell(
                          onTap: (){},
                          child:Container(
                              child:Row(
                                children: <Widget>[
                                  Icon(SeteaIcons.settings,color: Colors.white60),
                                  Padding(
                                    padding: EdgeInsets.only(left:6),
                                    child:Text('Настройки  |', style:GoogleFonts.rubik(textStyle: textStyle6),),)
                                ],
                              )
                          )
                      ),
                      InkWell(
                          onTap: (){},
                          child:Container(
                            padding: EdgeInsets.only(left:6),
                            child:Text('Выход', style:GoogleFonts.rubik(textStyle: textStyle6),),)

                      )

                    ],
                  )
              )
            ],
          ),// Populate the Drawer in the next step.
        ),),
      body: SizedBox.expand(
        child: PageView(
          controller: _pageController,
          onPageChanged: (index) {
            setState(() => _currentIndex = index);
          },
          children: <Widget>[
            Container(child:Cameraras() ),
            Container(child: Multimedia()),
            Container(child: Shop()),
            Container(child: Profile()),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavyBar(
        selectedIndex: _currentIndex,
        onItemSelected: (index) {
          setState(() => _currentIndex = index);
          _pageController.jumpToPage(index);
        },
        items: <BottomNavyBarItem> [
          BottomNavyBarItem(
            icon: Icon(SeteaIcons.scan),
            title: Text('Скан'),
            inactiveColor:Colors.black,
            activeColor: Color(0xFF273522),

          ),
          BottomNavyBarItem(
            icon: Icon(Setea2.vector__4_),
            title: Text('Настроение'),
            inactiveColor:Colors.black,
            activeColor: Color(0xFF273522),

          ),
          BottomNavyBarItem(
            icon: Icon(SeteaIcons.shop),
            title: Text('Магазин'),
            inactiveColor:Colors.black,
            activeColor: Color(0xFF273522),

          ),
          BottomNavyBarItem(
            icon: Icon(SeteaIcons.profile),
            title: Text('Профиль'),
            inactiveColor:Colors.black,
            activeColor: Color(0xFF273522),

          ),
        ],
      ),
    );
  }
}
